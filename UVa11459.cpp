#include <iostream>
using namespace std;

int main( void ) {

  int testcase = 0;
  cin >> testcase;

  int d;
  for( d = 0; d < testcase; d++ ) {

    int numberofplayers;
    int snakeladderboard[100];

    cin >> numberofplayers;

    int players[numberofplayers];

    int a;

    for( a = 0; a < numberofplayers; a++ )
      players[a] = 1;

    for( a = 0; a < 100; a++ )
      snakeladderboard[a] = 0;

    int numberofsnakesladders;
    cin >> numberofsnakesladders;
    int dierolls;
    cin >> dierolls;

    int b;
    int c;
    for( a = 0; a < numberofsnakesladders; a++ ) {
      cin >> b >> c;
      snakeladderboard[b] = c;
    }

    int whosturnisitanyway = 0;
    for( a = 0; a < dierolls; a++ ) {
      int move;
      cin >> move;
      if( whosturnisitanyway == numberofplayers )
        whosturnisitanyway = 0;
      players[whosturnisitanyway] = players[whosturnisitanyway] + move;

      if( snakeladderboard[players[whosturnisitanyway]] != 0 ) {
        players[whosturnisitanyway] = snakeladderboard[players[whosturnisitanyway]];
      }

      if( players[whosturnisitanyway] >= 100 ) {
        players[whosturnisitanyway] = 100;
        int e;

        for( e = a; e < dierolls - 1; e++ ) {
          int buffer;
          cin >> buffer;

        }
        break;
      }

      whosturnisitanyway++;

    }

    // cout << d << endl;
    for( a = 0; a < numberofplayers; a++ )
      cout << "Position of player " << a + 1 << " is " << players[a] << "." << endl;
  }
  return 0;
}
