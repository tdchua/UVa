#include <iostream>
#include <string>
#include <cstring>
#include <stack>

using namespace std;

int main( void ) {

  int testcasenumber;
  int i;
  char carrier;

  scanf( "%d", &testcasenumber );
  scanf( "%c", &carrier );

  for( i = 0; i < testcasenumber;  i++ ) {

    char mystring[128];
    int length;
    int j;
    stack<char> mystack;

    for( j = 0; ; j++ ) {

      scanf( "%c", &mystring[j] );
      if( mystring[j] == '\n' )
        break;

    }

    length = j;

    int counter1 = 0;
    int counter2 = 0;

    for( j = 0; j < length + 1; j++ ) {

        if( mystring[j] == '\n' && j == 0 ) {

          counter2 = 1;
          break;

        }

        if( mystring[j] == '(' ) {
          mystack.push( mystring[j] );
          counter1 = counter1 + 1;
        }

        if( mystring[j] == '[' ) {
          mystack.push( mystring[j] );
          counter1 = counter1 + 1;
        }

        if( mystring[j] == ')' && mystack.empty() != 1 ) {
          if( mystack.top() == '(')
            mystack.pop();
        }

        if( mystring[j] == ']' && mystack.empty() != 1 ) {
          if( mystack.top() == '[')
            mystack.pop();
        }

    }

    if ( ( mystack.empty() && counter1 != 0 ) || ( counter2 == 1 ) )
      cout << "Yes" << endl;
    else
      cout << "No" << endl;
  }
  return 0;
}
