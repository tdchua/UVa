#include <iostream>

using namespace std;

int main( void ) {

  int testcases;
  int testcasecount = 0;

  scanf( "%d", &testcases );

  for( testcasecount = 1; testcasecount <= testcases; testcasecount++ ) {

    int monsnum;
    int i;
    int max = 0;

    scanf( "%d", &monsnum );
    for( i = 0; i < monsnum; i++ ) {

      int speed;
      scanf( "%d", &speed );

      if( max < speed )
        max = speed;

    }

    cout << "Case " << testcasecount << ": " << max << endl;

  }
  return 0;
}
