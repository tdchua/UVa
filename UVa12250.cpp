#include <iostream>
#include <string>
using namespace std;

int main(void) {

  string input;
  int a = 1;
  while(cin >> input) {

    if(input == "#")
      break;

    if(input == "HELLO")
      printf("Case %d: ENGLISH\n", a);
    else if(input == "HOLA")
      printf("Case %d: SPANISH\n", a);
    else if(input == "HALLO")
      printf("Case %d: GERMAN\n", a);
    else if(input == "BONJOUR")
      printf("Case %d: FRENCH\n", a);
    else if(input == "CIAO")
      printf("Case %d: ITALIAN\n", a);
    else if(input == "ZDRAVSTVUJTE")
      printf("Case %d: RUSSIAN\n", a);
    else
      printf("Case %d: UNKNOWN\n", a);


    a++;
  }
  return 0;
}
