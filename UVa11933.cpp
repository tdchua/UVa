#include <iostream>
#include <bitset>
#include <cstdio>
using namespace std;

int main(void) {

  int input;

  while(cin >> input) {

    if(input == 0)
      break;

    bitset<32> bit_input(input);
    // cout << bit_input << endl;
    bitset<32> a(0);
    bitset<32> b(0);
    bool pattern = false;

    int c;
    for(c = 0; c < 32; c++) {
      // cout << bit_input[c] << " " << pattern << endl;
      if(bit_input[c] == true) {
        (pattern == false) ? a[c] = true : b[c] = true;
        pattern = !pattern;
      }
    }

    printf("%d %d\n", (int)a.to_ulong(), (int)b.to_ulong());
  }

  return 0;
}
