#include <iostream>
#include <string>
using namespace std;

int main( void ) {

  int L;

  while( scanf( "%d", &L ) == 1 ) {

    if( L == 0 )
      break;

    int i;
    string face = "+x";

    for( i = 0; i < L-1; i++ ) {

      string dir;
      cin >> dir;

      if( face == "+x" ) {
        if( dir == "+y" ) face = "+y";
        if( dir == "-y" ) face = "-y";
        if( dir == "+z" ) face = "+z";
        if( dir == "-z" ) face = "-z";
        if( dir == "No" ) face = face;
      }
      else if( face == "-x") {
        if( dir == "+y" ) face = "-y";
        if( dir == "-y" ) face = "+y";
        if( dir == "+z" ) face = "-z";
        if( dir == "-z" ) face = "+z";
        if( dir == "No" ) face = face;
      }
      else if( face == "+y") {
        if( dir == "+y" ) face = "-x";
        if( dir == "-y" ) face = "+x";
        if( dir == "+z" ) face = "+y";
        if( dir == "-z" ) face = "+y";
        if( dir == "No" ) face = face;
      }
      else if( face == "-y") {
        if( dir == "+y" ) face = "+x";
        if( dir == "-y" ) face = "-x";
        if( dir == "+z" ) face = "-y";
        if( dir == "-z" ) face = "-y";
        if( dir == "No" ) face = face;
      }
      else if( face == "+z") {
        if( dir == "+y" ) face = "+z";
        if( dir == "-y" ) face = "+z";
        if( dir == "+z" ) face = "-x";
        if( dir == "-z" ) face = "+x";
        if( dir == "No" ) face = face;
      }
      else if( face == "-z") {
        if( dir == "+y" ) face = "-z";
        if( dir == "-y" ) face = "-z";
        if( dir == "+z" ) face = "+x";
        if( dir == "-z" ) face = "-x";
        if( dir == "No" ) face = face;
      }
    }
    cout << face << endl;
  }
  return 0;
}
