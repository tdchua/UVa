#include <iostream>
#include <stack>
#include <string>

using namespace std;

int main( void ) {

  string firstline;
  string secondline;

  while( cin >> firstline >> secondline ) {

    stack<char> mystack;
    int i;

    for( i = 0; i < firstline.size(); i++ )
      mystack.push( firstline[ firstline.size() - 1 - i ]);

    for( i = 0; i < secondline.size(); i++ ) {

      if( mystack.empty() == false )
        if( mystack.top() == secondline[i] )
          mystack.pop();

    }

    if( mystack.empty() == true )
      cout << "Yes" << endl;
    else
      cout << "No" << endl;

  }
  return 0;
}
