#include <iostream>
#include <queue>
#include <string>
#include <utility>
#include <map>
using namespace std;

struct TimCompare {
  bool operator()(const pair<int, unsigned long long int>& A, const pair<int, unsigned long long int>& B) {

    if(A.second > B.second)
      return true;

    else if(A.second == B.second)
      return A.first > B.first;

    else
      return false;
  }
};



int main(void) {

  string input;
  priority_queue< pair<int, unsigned long long int>, vector<pair<int, unsigned long long int> >, TimCompare> my_pqueue;
  map<int, unsigned long long int> list_of_interval;

  while(cin >> input) {
    if(input == "#") {
      break;
    }

    int q_num;
    unsigned long long int period;

    cin >> q_num >> period;
    list_of_interval[q_num] = period;

    my_pqueue.push(make_pair(q_num, period));

  }

  int K;
  cin >> K;

  int a;
  for(a = 0; a < K; a++) {

    cout << my_pqueue.top().first << endl;
    my_pqueue.push(make_pair(my_pqueue.top().first,my_pqueue.top().second + list_of_interval[my_pqueue.top().first]));
    my_pqueue.pop();

  }

  return 0;
}
