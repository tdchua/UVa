#include <iostream>

using namespace std;

int numofdigits( int input ) {

  int digits = 0;

  for( ; input > 0; digits++ ) {
    input = input / 10;
  }

  return digits;
}

int * simplify( int numerator, int denominator ) {

  int a;
  static int simple[2];

  for( a = numerator; a > 1; a-- ) {
    if( numerator % a == 0 && denominator % a == 0 ) {
      numerator = numerator / a;
      denominator = denominator / a;
      a = numerator;
    }
  }
  simple[0] = numerator;
  simple[1] = denominator;

  return simple;
}


int main( void ) {

  int numberofinput;
  int numcase = 0;
  while( cin >> numberofinput ) {

    numcase++;

    if( numberofinput == 0 )
      break;

    int a;
    int sum = 0;
    for( a = 0; a < numberofinput; a++ ) {

      int input;
      cin >> input;
      sum = sum + input;

    }

    cout << "Case " << numcase << ":" << endl;

    if( sum % numberofinput == 0 ) {

      if( sum / numberofinput < 0 ) {
        cout << "- " << abs( sum / numberofinput ) << endl;
      }
      else
        cout << sum / numberofinput << endl;
    }

    else {

      int quotient;
      int numerator;
      int denominator;
      int * tosimplify;
      bool negative = false;


      quotient = sum / numberofinput;
      numerator = sum % numberofinput;
      denominator = numberofinput;

      if( numerator < 0 )
        negative = true;

      numerator = abs(sum) % numberofinput;
      tosimplify = simplify( numerator, denominator );

      numerator = tosimplify[0];
      denominator = tosimplify[1];

      int numofdigitsquotient = numofdigits(abs(quotient));
      int numofdigitsnumerator = numofdigits(numerator);
      int numofdigitsdenominator = numofdigits(denominator);

      // cout << "Quotient: " << quotient << endl;
      if( negative == true ) {

        int b;
        cout << "  ";

        if( quotient != 0 )
          for( b = 0; b < numofdigitsquotient; b++ ) {
            cout << " ";
          }

        for( b = 0; b < numofdigitsdenominator - numofdigitsnumerator; b++ ) {
          cout << " ";
        }
        cout << numerator << endl;
        cout << "- ";

        if( quotient != 0 )
          cout << abs( quotient );

        for( b = 0; b < numofdigitsdenominator; b++ ) {
          cout << "-";
        }

        cout << endl;

        cout << "  ";

        if( quotient != 0 )
          for( b = 0; b < numofdigitsquotient; b++ ) {
            cout << " ";
          }

        cout << denominator << endl;

      }

      else {

        int b;
        if( quotient != 0 )
          for( b = 0; b < numofdigitsquotient; b++ ) {
            cout << " ";
          }
        for( b = 0; b < numofdigitsdenominator - numofdigitsnumerator; b++ ) {
          cout << " ";
        }

        cout << numerator << endl;

        if( quotient != 0 )
          cout << abs( quotient );

        for( b = 0; b < numofdigitsdenominator; b++ ) {
          cout << "-";
        }

        cout << endl;
        if( quotient != 0 )
          for( b = 0; b < numofdigitsquotient; b++ ) {
            cout << " ";
          }
        cout << denominator << endl;

      }
    }
  }
  return 0;
}


// /*HK UST -> Nus -> NTU ->
