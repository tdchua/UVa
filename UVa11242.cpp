#include <iostream>
#include <array>
#include <vector>
#include <algorithm>
using namespace std;

int main( void ) {

  int f;
  int r;
  while ( cin >> f ) {
    if( f == 0 )
      break;

    cin >> r;
    double arrayoff[f];
    double arrayofr[r];

    int a;
    for( a = 0; a < f; a++ ) {
      cin >> arrayoff[a];
    }
    for( a = 0; a < r; a++ ) {
      cin >> arrayofr[a];
    }

    int prod = f * r;
    vector<double> driveratio;
    a = 0;
    int b = 0;
    int c = 0;
    for( b = 0; b < r; b++ ) {
      for( c = 0; c < f; c++ ) {
        driveratio.push_back((double)arrayofr[b] / (double)arrayoff[c]);
      }
    }
    sort( driveratio.begin(), driveratio.end() );
    double max = 0;
    for( a = 0; a < driveratio.size() - 1; a++ ) {
      if( driveratio[a+1] / driveratio[a] > max )
        max = driveratio[a+1] / driveratio[a];
    }
    // int buffer = int ( max * 1000 ) % 1000 % 10;
    // if( buffer == 5 )
    //   max = max + 0.01;
    printf( "%.2lf\n",max );
  }
  return 0;
}
