#include <iostream>
#include <map>
#include <vector>
#include <algorithm>
using namespace std;


int main(void) {

  int num_of_frosh;

  while(cin >> num_of_frosh) {

    if(num_of_frosh == 0)
      return 0;

    map<vector<int>, int> tally;

    int a;
    int b;
    int max_score = 0;
    int same_count = 1;
    for(b = 0; b < num_of_frosh; b++ ) {

      vector<int> frosh_picks;
      for(a = 0; a < 5; a++) {

        int pick;
        cin >> pick;

        frosh_picks.push_back(pick);

      }

      sort(frosh_picks.begin(), frosh_picks.end());

      tally[frosh_picks] = tally[frosh_picks] + 1;
      if(max_score < tally[frosh_picks]) {
        max_score = tally[frosh_picks];
        same_count = 1;
      }
      else if(max_score == tally[frosh_picks]) {
        same_count += 1;
      }

    }

    cout << max_score * same_count << endl;
  }
  return 0;
}
