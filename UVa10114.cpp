#include <iostream>

using namespace std;

struct deprecord {

  int when;
  double dep;

};

int main( void ){

  double loanmonths;

  while( scanf( "%lf", &loanmonths ) ) {

    if( loanmonths < 0 )
      break;

    double downpayment;
    double amountofloan;
    int numberofdeprecords;

    scanf( "%lf", &downpayment );
    scanf( "%lf", &amountofloan );
    scanf( "%d", &numberofdeprecords );

    struct deprecord array[numberofdeprecords];
    int i;

    for( i = 0; i < numberofdeprecords; i++ ) {

      scanf( "%d", &array[i].when );
      scanf( "%lf", &array[i].dep );

    }

    double totalamount = downpayment + amountofloan;
    double currentpricedep = totalamount;
    double currentpricepay = amountofloan;
    double paypermonth = amountofloan / loanmonths;
    double depvalue = 0;

    for( i = 0; i < loanmonths + 1; i++ ) {

      int j;

      for( j = 0; j < numberofdeprecords; j++ ) {

        if( i == array[j].when )
          depvalue = array[j].dep;
      }

      currentpricedep = currentpricedep * ( 1.00 - depvalue);
      // printf( "%lf\n", currentpricedep );

      if( i >= 1 )
        currentpricepay = currentpricepay - paypermonth;

      // printf( "%lf\n", currentpricepay );

      if( currentpricepay < currentpricedep ) {

        if( i == 1 ) {
          cout << "1 month" << endl;
          break;
        }
        else {
          cout << i << " months" << endl;
          break;
        }

      }


    }
  }

  return 0;
}
