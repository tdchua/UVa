#include <iostream>
#include <string>

using namespace std;

char first[] = { '`', '1', '2', '3', '4', '5', '6', '7', '8', '9', '0', '-', '=' };
char second[] = { 'Q', 'W', 'E', 'R', 'T', 'Y', 'U', 'I', 'O', 'P', '[', ']', '\\' };
char third[] = { 'A', 'S', 'D', 'F', 'G', 'H', 'J', 'K', 'L', ';' , '\'' };
char fourth[] = { 'Z', 'X' , 'C', 'V', 'B', 'N', 'M', ',', '.', '/' };
string input;
int lengthofstr;
int lengthofkey;
int i,j;


int main(void) {

  while( getline( cin, input) ) {
    string output;
    lengthofstr = input.size();

    for ( i = 0; i < lengthofstr; i++ ) {
      if ( input[i] == ' ' )
        output.push_back( ' ' );
      for ( j = 0; j < sizeof(first) ; j++ ) {

        if ( input[i] == first[j] )
          output.push_back( first[j-1] );

      }

      for ( j = 0; j < sizeof(second) ; j++ ) {

        if ( input[i] == second[j] )
          output.push_back( second[j-1] );

      }

      for ( j = 0; j < sizeof(third) ; j++ ) {

        if ( input[i] == third[j] )
          output.push_back( third[j-1] );

      }

      for ( j = 0; j < sizeof(fourth) ; j++ ) {

        if ( input[i] == fourth[j] )
          output.push_back( fourth[j-1] );

      }
    }

    cout << output << endl;
  }
}
