#include <iostream>
using namespace std;

int main( void ) {

  int testcase;
  cin >> testcase;

  int a;

  for( a = 0; a < testcase; a++ ) {

    int rows;
    cin >> rows;

    int columns;
    cin >> columns;

    int days;
    cin >> days;

    char grid[rows][columns];
    bool flipper[rows][columns];

    int b;
    int c;

    for( b = rows - 1; b >= 0; b-- )
      for( c = 0; c < columns; c++ ) {
        cin >> grid[b][c];
        flipper[b][c] = 0;
      }
    // for( b = 0; b < rows; b++ ) {
    //   for( c = 0; c < columns; c++ )
    //     cout << grid[b][c];
    //   cout << endl;
    // }
    int e;
    for( e = 0; e < days; e++ ) {
      // cout << "Hi";
      for( b = 0; b < rows; b++ )
        for( c = 0; c < columns; c++ )
          flipper[b][c] = 0;
      for( b = 0; b < rows; b++ )
        for( c = 0; c < columns; c++ ) {
          if( b != 0 ) {
            switch( grid[b][c] ) {
              case 'S' :
                if( grid[b-1][c] == 'R' )
                  flipper[b][c] = 1;
                break;
              case 'R' :
                if( grid[b-1][c] == 'P' )
                  flipper[b][c] = 1;
                break;
              case 'P' :
                if( grid[b-1][c] == 'S' )
                  flipper[b][c] = 1;
                break;
            }
          }
          if( b != rows - 1 ) {
            switch( grid[b][c] ) {
              case 'S' :
                if( grid[b+1][c] == 'R' )
                  flipper[b][c] = 1;
                break;
              case 'R' :
                if( grid[b+1][c] == 'P' )
                  flipper[b][c] = 1;
                break;
              case 'P' :
                if( grid[b+1][c] == 'S' )
                  flipper[b][c] = 1;
                break;
            }
          }

          if( c != 0 ) {
            switch( grid[b][c] ) {
              case 'S' :
                if( grid[b][c-1] == 'R' )
                  flipper[b][c] = 1;
                break;
              case 'R' :
                if( grid[b][c-1] == 'P' )
                  flipper[b][c] = 1;
                break;
              case 'P' :
                if( grid[b][c-1] == 'S' )
                  flipper[b][c] = 1;
                break;
            }
          }

          if( c != columns - 1 ) {
            switch( grid[b][c] ) {
              case 'S' :
                if( grid[b][c+1] == 'R' )
                  flipper[b][c] = 1;
                break;
              case 'R' :
                if( grid[b][c+1] == 'P' )
                  flipper[b][c] = 1;
                break;
              case 'P' :
                if( grid[b][c+1] == 'S' )
                  flipper[b][c] = 1;
                break;
            }
          }
        }

      for( b = 0; b < rows; b++ )
        for( c = 0; c < columns; c++ ) {
          if( flipper[b][c] == 1 )
            switch( grid[b][c] ) {
              case 'S' :
                grid[b][c] = 'R';
                break;
              case 'R' :
                grid[b][c] = 'P';
                break;
              case 'P' :
                grid[b][c] = 'S';
                break;
            }
        }
    }
    if( a != 0 )
      cout << endl;
    for( b = rows - 1; b >= 0; b-- ) {
      for( c = 0; c < columns; c++ )
        cout << grid[b][c];
      cout << endl;
    }
  }
  return 0;
}
