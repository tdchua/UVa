#include <iostream>
#include <string>
using namespace std;

int main(void) {

  string input;
  int testcase = 1;
  while(cin >> input) {

    if(input == "*")
      break;

    if(input == "Hajj")
      printf("Case %d: Hajj-e-Akbar\n", testcase);
    else
      printf("Case %d: Hajj-e-Asghar\n", testcase);

    testcase++;
  }
  return 0;
}
