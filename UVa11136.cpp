#include <iostream>
#include <vector>
#include <deque>
#include <algorithm>
#include <set>

using namespace std;

int main(void) {

  int promotion_days;
  while(scanf("%d", &promotion_days) == 1) {

    if(promotion_days == 0)
      break;

    else {

      int a;
      unsigned long long int total_expense = 0;
      multiset<int> bills;
      for(a = 0; a < promotion_days; a++) {


        int number_of_bills;
        scanf("%d", &number_of_bills);

        int b;
        int expense = 0;
        for(b = 0; b < number_of_bills; b++) {

          int bill;
          scanf("%d", &bill);
          bills.insert(bill);

        }

        // cout << bills.size() << endl;
        int saved = *(--bills.end()) - *(bills.begin());
        // cout << "Saved: " << saved << endl;
        bills.erase(--bills.end());
        bills.erase(bills.begin());
        total_expense += saved;


      }
      printf("%llu\n", total_expense);
    }
  }
  return 0;
}
