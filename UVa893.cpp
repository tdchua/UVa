#include <iostream>

using namespace std;
int numberofleapyears = 0;
bool yesornoleap( int year ) {
  if(( year % 4 == 0 ) && ( year % 400 == 0 || year % 100 != 0 )) {
    numberofleapyears++;
    // cout << "LEAP: " << year << endl;
    return true;
  }
  else
    return false;
}


int main( void ) {

  unsigned int predict;
  while( scanf("%u", &predict) == 1 ) {

    unsigned int day,month,year;
    unsigned int dayspermonth[13] = { 0, 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31 };
    unsigned int leapdayspermonth[13] = { 0, 31, 29, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31 };
    unsigned int tousedayspermonth[13] = { 0, 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31 };


    scanf("%u", &day);
    scanf("%u", &month);
    scanf("%u", &year);


    if( day == 0 && month == 0 && year == 0 && predict == 0 ) {
      break;
    }

    unsigned int incrementyear;
    unsigned int incrementday;
    unsigned int incrementmonth;
    unsigned int daysfornewyear = 0;
    unsigned int leapyear = 0;

    //standardize to January 1 first
    if( ( year % 4 == 0 ) && ( year % 400 == 0 || year % 100 != 0 )) {
      leapyear = 1;
      // cout << "LEAP" << endl;
    }

    int i = 1;

    for( i = 1; i < 13; i++ ) {
      if( leapyear == 1 ) {

        if( i == month ) {
          daysfornewyear = daysfornewyear + ( leapdayspermonth[i] - day );
        }
        else if( i > month ) {
          daysfornewyear = daysfornewyear + leapdayspermonth[i];
        }

      }
      else if( leapyear == 0 ) {

        if( i == month ) {
          daysfornewyear = daysfornewyear + ( dayspermonth[i] - day );
        }
        else if( i > month ) {
          daysfornewyear = daysfornewyear + dayspermonth[i];
        }

      }
    }
    if( leapyear == 1 )
      for( i = 0; i < 13; i++ ) {
        tousedayspermonth[i] = leapdayspermonth[i];
      }


    if( predict < daysfornewyear ) {
      for( i = 1; i < 13; i++ ) {
        if( i == month ) {

          if( predict <= tousedayspermonth[i] - day ) {
            cout << day + predict << ' ' << month << ' ' << year << endl;
            predict = 0;
            break;
          }
          else if( predict > tousedayspermonth[i] - day ) {
            predict = predict - tousedayspermonth[i] + day;
            incrementmonth++;
          }

        }
        else if( i > month && predict > 0 ) {
          if( predict <= tousedayspermonth[i] ) {
            cout << predict << ' ' << i << ' ' << year << endl;
            predict = 0;
            break;
          }

          else {
            predict = predict - tousedayspermonth[i];
            incrementmonth++;
          }
        }
      }
    }
    else if( predict == daysfornewyear ) {
      cout << "31 12 " << year << endl;
      predict = 0;
    }
    else if( predict > daysfornewyear ) {
      predict = predict - daysfornewyear;
      year = year + 1;
      month = 1;
      day = 0;
      while( predict > 0 ) {
        // cout << year << endl;
        if( yesornoleap( year )) {
          daysfornewyear = 366;
          for( i = 0; i < 13; i++ )
            tousedayspermonth[i] = leapdayspermonth[i];
        }


        if( !yesornoleap( year )) {
          daysfornewyear = 365;
          for( i = 0; i < 13; i++ )
            tousedayspermonth[i] = dayspermonth[i];
        }

        if( predict > daysfornewyear ) {
          year++;
          predict = predict - daysfornewyear;
          continue;
        }

        if( predict == daysfornewyear ) {
          cout << "31 12 " << year << endl;
          predict = 0;
          break;
        }

        if( predict < daysfornewyear ) {
          month = 1;
          for( i = 1; i < 13; i++ ) {
            if( predict <= tousedayspermonth[i] ) {
              cout << predict << ' ' << i << ' ' << year << endl;
              predict = 0;
              break;
            }
            if( predict > tousedayspermonth[i] ) {
              predict = predict - tousedayspermonth[i];
            }
          }
        }
      }
    }
  }
  return 0;
}
