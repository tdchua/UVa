#include <iostream>
using namespace std;

int main()
{
    long input1;
    long input2;

    while (cin >> input1 >> input2)
    {
        if(cin.eof())
            return 0;
        cout << abs(input2-input1) << endl;
    }
    return 0;
} 