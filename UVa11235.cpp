#include <iostream>
#include <vector>
#include <algorithm>
#include <math.h>
using namespace std;

int mid_point(int start, int end) {

  return start + (end - start) / 2;
}

int query_tree(int ss, int se, int* st, int si, int qs, int qe) {
  // cout << qs << " " << qe << endl;
  if(ss >= qs && se <= qe)
    return st[si];

  if(ss > qe || se < qs )
    return 0;

  int mid = mid_point(ss, se);
  int answer;
  //case it overlaps
  answer = max(query_tree(ss, mid, st, 2 * si + 1, qs, qe), query_tree(mid + 1, se, st, 2 * si + 2, qs, qe));
  return answer;
}


int build_tree(vector<int> &tim_vec, int ss, int se, int* st, int si) {

  if(ss == se) {
    st[si] = 1;
    return 1;
  }

  int mid = mid_point(ss, se);
  build_tree(tim_vec, ss, mid, st, 2 * si + 1);
  build_tree(tim_vec, mid + 1, se, st, 2 * si + 2);

  int a;
  int max = 0;
  int buffer = 0;
  int counter = 0;
  for(a = ss; a <= se; a++) {
    if(a == ss)
      buffer = tim_vec[a];

    if(buffer == tim_vec[a]) {
      counter++;
      // cout << "Buffer: " << tim_vec[a] << endl;
    }

    if(buffer != tim_vec[a] || a == se) {
      buffer = tim_vec[a];
      if(max < counter)
        max = counter;
      counter = 1;
    }
  }
  st[si] = max;
  // for(a = ss; a <= se; a++) {
  //   cout << tim_vec[a] << " ";
  // }
  // cout << endl;
  cout << "Lower: " << ss << " Upper: " << se << " Answer: " << max << endl;
  return st[si];
}


int main(void) {

  int n;
  int q;

  while(cin >> n) {

    if(n == 0)
      break;

    cin >> q;
    int a;
    vector<int> tim_vec;
    vector<int> tally;
    int input;
    int buffer = 0;
    int counter = 0;

    for(a = 0; a < n; a++) {
      cin >> input;
      tim_vec.push_back(input);
    }

    int N = tim_vec.size();
    int height = (int)ceil(log2(N));
    int max_size = 2 * (int)pow(2, height) - 1;
    int *st = new int[max_size];


    build_tree(tim_vec, 0, N - 1, st, 0);

    int lower_bound;
    int upper_bound;

    for(a = 0; a < q; a++) {

      cin >> lower_bound >> upper_bound;
      lower_bound -= 1;
      upper_bound -= 1;

      cout << query_tree(0, N - 1, st, 0, lower_bound, upper_bound) << endl;

    }

  }
  return 0;
}
