#include <iostream>
#include <string>
#include <sstream>
#include <map>
#include <set>
#include <algorithm>
#include <vector>
#include <iterator>
#include <queue>
using namespace std;

int main(void) {

  string input;
  set<char> sleeping_areas;
  map<char, set<char> > sleeping_neighbors;
  map<char, set<char> > woke_neighbors;
  stringstream ss;
  while(getline(cin, input)) {
    sleeping_areas.clear();
    sleeping_neighbors.clear();
    woke_neighbors.clear();

    int slept_areas;
    slept_areas = stoi(input);

    getline(cin, input);
    int M;
    M = stoi(input);

    getline(cin, input);

    set<char> woke_areas;
    int a;
    for(a = 0; a < input.size(); a++)
      woke_areas.insert(input[a]);

    while(getline(cin, input)) {
      if(input.size() == 0)
        break;

      else {
        //for input[0]
        sleeping_areas.insert(input[0]);
        sleeping_areas.insert(input[1]);

        if(woke_areas.find(input[1]) == woke_areas.end()) //2nd character is awake
          sleeping_neighbors[input[0]].insert(input[1]);
        else
          woke_neighbors[input[0]].insert(input[1]);
        //for input[1]
        if(woke_areas.find(input[0]) == woke_areas.end())
          sleeping_neighbors[input[1]].insert(input[0]);
        else
          woke_neighbors[input[1]].insert(input[0]);

      }
    }

    int time_now = 0;
    set<char> difference;

    if(sleeping_areas.empty() == false && woke_areas.empty() == false) {
      set_difference(sleeping_areas.begin(), sleeping_areas.end(), woke_areas.begin(), woke_areas.end(), std::inserter(difference, difference.end()));
      sleeping_areas = difference;
    }

    if(sleeping_areas.size() + woke_areas.size() != slept_areas) {
      printf("THIS BRAIN NEVER WAKES UP\n");
      continue;
    }

    queue<char> changed;
    for(time_now = 0; sleeping_areas.empty() == false; ++time_now) {

      bool something_changed = false;

      //updating neighbors
      while(changed.empty() == false) {
        //now i have to iterate through all of the neighbors of the nodes who just woke up
        for(auto b = sleeping_neighbors[changed.front()].begin(); b != sleeping_neighbors[changed.front()].end(); ++b) {
          if(sleeping_neighbors.find(*b) != sleeping_neighbors.end()) {
            if(sleeping_neighbors[*b].find(changed.front()) != sleeping_neighbors[*b].end()) {
              sleeping_neighbors[*b].erase(changed.front());
            }
          }

          woke_neighbors[*b].insert(changed.front());
        }
        changed.pop();
      }

      //waking up nodes
      if(sleeping_areas.empty() == false) {
        for(auto b = sleeping_areas.begin(); b != sleeping_areas.end(); ) {
          // cout << "Processing: " << *b << endl;
          if(woke_neighbors[*b].size() >= 3) {
            if(sleeping_areas.find(*b) != sleeping_areas.end()) {
              sleeping_areas.erase(*b);
              woke_areas.insert(*b);
              changed.push(*b);
              // cout << "am inserting: " << *b << endl;
              b = sleeping_areas.begin();
            }
            something_changed = true;
          }
          else {
            b++;
          }
          if(sleeping_areas.empty() == true) {
            // cout << "Bye" << endl;
            break;
          }
        }
      }

      if(something_changed == false) {
        break;
      }
    }
    if(sleeping_areas.empty() == true)
      printf("WAKE UP IN, %d, YEARS\n", time_now);
    else
      printf("THIS BRAIN NEVER WAKES UP\n");

  }
  return 0;
}
