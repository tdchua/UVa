

#include <cstdio>
using namespace std;


int main( ) {

  int numberofsoldiers;
  int numberofreports;

  while( scanf("%d %d", &numberofsoldiers, &numberofreports), numberofsoldiers || numberofreports ) {
    /* If i put the initializatino inside this, the code would get WA */
    int battalionleft[100005];
    int battalionright[100005];
    int a;

    for( a = 0; a < 100005; a++ ) {
      battalionleft[a] = 0;
      battalionright[a] = 0;
    }

    for( a = 1; a <= numberofsoldiers; a++ ) {
      battalionleft[a] = a - 1;
      battalionright[a] = a + 1;
    }

    battalionleft[1] = -1;
    battalionright[numberofsoldiers] = -1;

    int leftkill;
    int rightkill;
    for( a = 0; a < numberofreports; a++ ) {

      scanf("%d %d", &leftkill, &rightkill);

      battalionleft[battalionright[rightkill]] = battalionleft[leftkill];
      if( battalionleft[leftkill] != -1 )
        printf("%d", battalionleft[leftkill]);
      else
        printf("*");

      battalionright[battalionleft[leftkill]] = battalionright[rightkill];
      if( battalionright[rightkill] != -1 )
        printf(" %d\n", battalionright[rightkill]);
      else
        printf(" *\n");

    }
    printf("-\n");
  }
  return 0;
}
