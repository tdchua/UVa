#include <iostream>
#include <stack>
#include <string>

using namespace std;

int main(void) {

  string input;
  int case_number = 1;

  while(cin >> input) {

    if(input == "end")
      break;

    stack<char> my_stack[1000];
    int number_of_stacks_used = 0;
    int a;

    for(a = 0; a < input.length(); a++) {

      int b;
      bool placed = false;

      for(b = 0; b < number_of_stacks_used; b++) {

        // cout << my_stack[b].top() << " " << input[a] << " " << my_stack[b].top() - input[a] << endl;

        if(my_stack[b].top() - input[a] >= 0) {
          my_stack[b].push(input[a]);
          placed = true;
          break;
        }

      }

      if(placed == false) {
        number_of_stacks_used++;
        my_stack[number_of_stacks_used - 1].push(input[a]);
      }

    }

    cout << "Case " << case_number << ":" << " " << number_of_stacks_used << endl;
    case_number = case_number + 1;
  }
  return 0;
}
