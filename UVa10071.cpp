#include <iostream>
#include <cstdlib>
using namespace std;

int main()
{
    long long int A,B;
    long long int distance;
    while(cin >> A >> B)
    {   
        if(cin.eof())
            return 0;
            
        distance = A * B * 2;
        cout << distance <<endl;
    }
    return 0;
}