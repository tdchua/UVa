#include <iostream>
#include <math.h>
#include <string>
// #include <fstream>

using namespace std;

int main( void ) {

  int noofinputs;
  int testcaseno = 0;
  // ofstream myfile;
  // myfile.open ("UVaAnswers.txt");

  while( cin >> noofinputs ) {

    if( noofinputs == 0 )
      break;

    testcaseno++;

    if( testcaseno != 1 )
      cout << endl;

    cout << "Case " << testcaseno << ':' << endl;


    string firstinput;
    cin >> firstinput;
    cout << firstinput;
    string mostrecentstreakinput;

    if( noofinputs == 1 ) {
      cout << endl;
    }

    int firstinputval = 0;
    int a;
    double multiplier;

    for( a = 1; a < firstinput.length(); a++ ) {
      multiplier = pow( 10, firstinput.length() - a - 1);
      firstinputval = firstinputval + ( firstinput[a] - '0' ) * multiplier;
    }

    int b = 1;
    int diff = 1;
    bool streak = false;

    for( b = 1; b < noofinputs; b++ ) {
      // cout << b << endl;
      string compareinput;
      cin >> compareinput;
      int compareinputval = 0;

      for( a = 1; a < firstinput.length(); a++ ) {
        multiplier = pow( 10, firstinput.length() - a - 1);
        compareinputval = compareinputval + ( compareinput[a] - '0' ) * multiplier;
      }

      if( compareinputval - firstinputval == diff ) {
        diff = diff + 1;
        streak = true;
        // cout << "I went in here" << endl;
        if( b == noofinputs - 1 ) {
          cout << '-';
          int c;
          int d;
          for( c = 0; c < firstinput.length(); c++ ) {
            if( firstinput[c] != compareinput[c] ) {
              for( d = c; d < firstinput.length(); d++ ) {
                cout << compareinput[d];
              }
              break;
            }
          }
          cout << endl;
          streak = false;
        }
        mostrecentstreakinput = compareinput;
      }

      else {
        if( streak == false ) {
          cout << endl;
          firstinputval = compareinputval;
          firstinput = compareinput;
          cout << firstinput;
          if( b == noofinputs - 1 )
            cout << endl;
        }
        else {
          cout << '-';
          int c;
          int d;
          for( c = 0; c < firstinput.length(); c++ ) {
            if( firstinput[c] != mostrecentstreakinput[c] ) {
              for( d = c; d < firstinput.length(); d++ ) {
                cout << mostrecentstreakinput[d];
              }
              break;
            }
          }
          cout << endl;
          firstinput = compareinput;
          firstinputval = compareinputval;
          cout << compareinput;
          streak = false;
          diff = 1;
          if( b == noofinputs - 1 )
            cout << endl;
        }
      }
    }
  }
  cout << endl;
  // cout.close();
  return 0;
}
