#include <iostream>
#include <vector>
#include <map>
using namespace std;


int main(void) {

  int num_of_elements;
  int num_of_queries;

  while(cin >> num_of_elements >> num_of_queries) {

    map<int, int> my_map;
    vector<vector<int> > my_vector;

    int a;
    for(a = 0; a < num_of_elements; a++) {
      int input;
      cin >> input;

      if(my_map.find(input) == my_map.end()) {
        my_map[input] = (int)my_vector.size();
        vector<int> insert_this;
        insert_this.push_back(a + 1);
        my_vector.push_back(insert_this);
        // cout << "My_Vector Size: " << my_vector.size() << endl;
      }
      else {
        // cout << "Found it!" << endl;
        // cout << "My_Map[input]: " << my_map[input] << endl;
        my_vector[my_map[input]].push_back(a + 1);

      }
    }

    for(a = 0; a < num_of_queries; a++) {
      int k, v; //kth occurrence of v
      cin >> k >> v;
      if(my_map.find(v) == my_map.end()) {
        cout << "0" << endl;
      }
      else {
        if(my_vector[my_map[v]].size() < k)
          cout << "0" << endl;

        else {
          cout << my_vector[my_map[v]][k-1] << endl;
        }
      }
    }
  }
  return 0;
}
