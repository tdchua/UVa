#include <iostream>
#include <string>
#include <ctype.h>

using namespace std;

int main( void ) {

  string input;

  while( getline( cin, input ) ) {

    if( input == "DONE" )
      break;

    int a;

    for( a = 0; a < input.length(); a++ ) {
      if( input[a] == '.' || input[a] == ',' || input[a] == '!' || input[a] == '?' || input[a] == ' ' ) {
        input.erase(a,1);
        a = -1;
      }
    }
    bool palindrome = true;
    int b = input.length() - 1;
    // cout << input << endl;
    for( a = 0; a < input.length() / 2; a++ ) {
      if((char) tolower( input[a] ) != (char) tolower( input[b] ))
        palindrome = false;
      b--;
    }
    if( palindrome == true )
      cout << "You won't be eaten!" << endl;
    else
      cout << "Uh oh.." << endl;
  }
  return 0;
}
