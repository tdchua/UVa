#include <iostream>
#include <string>
using namespace std;

int main( void ) {

  int testcases;
  int a;
  cin >> testcases;
  for( a = 0; a < testcases; a++ ) {
    if( a != 0 )
      cout << endl;
    int numberofmanufacturers;
    cin >> numberofmanufacturers;

    string manufacturer[numberofmanufacturers];
    int lowprice[numberofmanufacturers];
    int highprice[numberofmanufacturers];
    int b;

    for( b = 0; b < numberofmanufacturers; b++ ) {
      cin >> manufacturer[b] >> lowprice[b] >> highprice[b];
    }

    int query;
    int numberofquery;
    cin >> numberofquery;

    for( b = 0; b < numberofquery; b++ ) {

      cin >> query;
      int accepted = 0;
      int index = 0;

      for( int c = 0; c < numberofmanufacturers; c++ ) {
        if( query >= lowprice[c] && query <= highprice[c] ) {
          accepted++;
          index = c;
        }
      }
      if( accepted == 1 )
        cout << manufacturer[index] << endl;
      else
        cout << "UNDETERMINED" << endl;
    }
  }
  return 0;
}
