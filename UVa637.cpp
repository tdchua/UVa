#include <iostream>
using namespace std;
struct page {
  int left;
  int right;
};

int main( void ) {

  int limit;

  while( cin >> limit ) {

    if( limit == 0 )
      break;

    int pages;
    pages = limit / 4;

    // cout << "Limit: " << pages << endl;
    if( limit % 4 > 0 )
      pages++;

    page book[(pages * 2) + 1];

    bool leftorright = true; //true = right, false = left
    bool reachtheend = false;
    int flippage;
    int a;
    for( a = 1; a <= pages * 2; a++ ) {
      book[a].left = 0;
      book[a].right = 0;
    }
    for( a = 1, flippage = 1; a <= limit; a++ ) {
      // cout << reachtheend << endl;
      if( leftorright == true ) {
        book[flippage].right = a;
        leftorright = !leftorright;
      }
      else if( leftorright == false ) {
        book[flippage].left = a;
        leftorright = !leftorright;
      }

      if( flippage ==  pages * 2 && reachtheend == false ) {
        reachtheend = true;
        continue;

      }
      if( reachtheend == false )
        flippage++;
      else if( reachtheend == true ) {
        // cout << "lol" << endl;
        flippage--;
      }

    }
    int whichpage = 1;
    int counttoflip = 0;
    bool frontorback = true; // true = front, false = back
    cout << "Printing order for " << limit << " pages:" << endl;
    if( limit <= 1 ) {
      cout << "Sheet 1, front: ";
      if( book[a].left == 0 )
        cout << "Blank";
      else
        cout << book[a].left;
      cout << ", " << book[1].right << endl;
    }

    else {
      // cout << book[2].left << " " << book[2].right << endl;
      for( a = 1; a <= pages * 2; a++, counttoflip++ ) {
        // if( counttoflip == 0 )
        cout << "Sheet " << whichpage << ", ";
        if( frontorback == true ) {
          cout << "front: ";
          frontorback = !frontorback;
        }
        else {
          cout << "back : ";
          frontorback = !frontorback;
        }
        if( book[a].left == 0 )
          cout << "Blank";
        else
          cout << book[a].left;
        cout << ", ";
        if( book[a].right == 0 )
          cout << "Blank" << endl;
        else
          cout << book[a].right << endl;
        if( counttoflip == 1 ) {
          whichpage++;
          counttoflip = -1;
        }
        // cout << book[a].left << " " << book[a].right << endl;
      }
    }
  }
  return 0;
}
