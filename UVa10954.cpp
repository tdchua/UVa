#include <iostream>
#include <queue>
#include <functional>
#include <vector>
using namespace std;

int main(void) {

  int N;

  while(scanf("%d", &N)) {

    if(N == 0)
      break;

    priority_queue<int, vector<int> ,greater<int> > my_pqueue;
    int a;
    int input;

    for(a = 0; a < N; a++) {
      scanf("%d", &input);
      my_pqueue.push(input);
    }

    long long unsigned int sum = 0;
    int i = 0;
    long long unsigned int score = 0;

    while(my_pqueue.empty() == false) {
      int b;
      int c;

      b = my_pqueue.top();
      my_pqueue.pop();
      c = my_pqueue.top();
      my_pqueue.pop();

      // cout << "B: " << b << " C: " << c << endl;

      sum = b + c;
      if(my_pqueue.empty() == false)
        my_pqueue.push(sum);
      score += sum;


    }
    printf("%lld\n", score);
  }
  return 0;
}
