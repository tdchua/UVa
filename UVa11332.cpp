#include <iostream>

using namespace std;


int digitnum( int input ) {

  int digitno = 0;

  while( input > 0 ) {

    input = input / 10;
    digitno = digitno + 1;

  }

  return digitno;
}


int main( void ) {

  int input;

  while( scanf( "%d", &input) == 1 ) {

    if( input == 0 )
      break;


    while( digitnum(input) > 1 ) {

      int carrier = input;
      int sum = 0;

      while( carrier > 0 ) {

        sum = sum + ( carrier % 10);
        carrier = carrier / 10;

      }
      input = sum;

    }

    cout << input << endl;
  }

  return 0;
}
