#include <iostream>

using namespace std;

int main( void ) {

  int testcase;

  cin >> testcase;

  int a;

  for( a = 0; a < testcase; a++ ) {

    char chesspiece;
    int rows;
    int columns;
    cin >> chesspiece >> rows >> columns;

    char chessboard[rows][columns];

    int b;
    int c;

    // e = empty, x = invalid, o = used by a piece
    for( b = 0; b < rows; b++ )
      for( c = 0; c < columns; c++ )
        chessboard[b][c] = 'e';


    if( chesspiece == 'r' ) { // rook
      if( rows > columns )
        cout << columns << endl;
      else
        cout << rows << endl;

      continue;
    }

    else if( chesspiece == 'k') {//knight

      for( b = 0; b < rows; b++ )
        for( c = 0; c < columns; c++ ) {
          if( chessboard[b][c] == 'e' ){
            chessboard[b][c] = 'o';

            int d;
            for( d = 1; d < 3; d++ ) {

              if( c + d < columns ) {

                if( d == 2 ) {
                  if( b + 1 < rows )
                    chessboard[b+1][c+d] = 'x';
                  if( b - 1 >= 0 )
                    chessboard[b-1][c+d] = 'x';
                }
              }

              if( c - d >= 0 ) {

                if( d == 2 ) {
                  if( b + 1 < rows )
                    chessboard[b+1][c-d] = 'x';
                  if( b - 1 >= 0 )
                    chessboard[b-1][c-d] = 'x';
                }
              }

              if( b + d < rows ) {

                if( d == 2 ) {
                  if( c + 1 < columns )
                    chessboard[b+d][c+1] = 'x';
                  if( c - 1 >= 0 )
                    chessboard[b+d][c-1] = 'x';
                }
              }

              if( b - d >= 0 ) {
                
                if( d == 2 ) {
                  if( c + 1 < columns )
                    chessboard[b+d][c+1] = 'x';
                  if( c - 1 >= 0 )
                    chessboard[b+d][c-1] = 'x';
                }
              }
            }
          }
        }
    }

    else if( chesspiece == 'K') {// king

      for( b = 0; b < rows; b++ )
        for( c = 0; c < columns; c++ ) {
          if( chessboard[b][c] == 'e' ) {
            chessboard[b][c] = 'o';
            if( c + 1 < columns )
              chessboard[b][c+1] = 'x';
            if( c + 1 < columns && b + 1 < rows )
              chessboard[b+1][c+1] = 'x';
            if( b + 1 < rows )
              chessboard[b+1][c] = 'x';
            if( c - 1 >= 0 )
              chessboard[b][c-1] = 'x';
            if( b - 1 >= 0 )
              chessboard[b-1][c] = 'x';
            if( c - 1 >= 0 && b - 1 >= 0 )
              chessboard[b-1][c-1] = 'x';
            if( c - 1 >= 0 && b + 1 < rows )
              chessboard[b+1][c-1] = 'x';
            if( c + 1 < columns && b - 1 >= 0 )
              chessboard[b-1][c+1] = 'x';

          }
        }
    }

    else if( chesspiece == 'Q' ) {
      int e;
      for( b = 0; b < rows; b++ )
        for( c = 0; c < columns; c++ ) {
          if( chessboard[b][c] == 'e' ) {
            chessboard[b][c] = 'o';
            for( e = 1; e < max(rows,columns); e++ ) {
              if( c + e < columns )
                chessboard[b][c+e] = 'x';
              if( c + e < columns && b + e < rows )
                chessboard[b+e][c+e] = 'x';
              if( b + e < rows )
                chessboard[b+e][c] = 'x';
              if( c - e >= 0 )
                chessboard[b][c-e] = 'x';
              if( b - e >= 0 )
                chessboard[b-e][c] = 'x';
              if( c - e >= 0 && b - e >= 0 )
                chessboard[b-e][c-e] = 'x';
              if( c - e >= 0 && b + e < rows )
                chessboard[b+e][c-e] = 'x';
              if( c + e < columns && b - e >= 0 )
                chessboard[b-e][c+e] = 'x';
            }
          }
        }
    }

    int count = 0;
    for( b = 0; b < rows; b++ )
      for( c = 0; c < columns; c++ )
        if( chessboard[b][c] == 'o' )
          count++;

    cout << count << endl;
    for( b = 0; b < rows; b++ ) {
      for( c = 0; c < columns; c++ )
        cout << chessboard[b][c];
      cout << endl;
    }
  }
  return 0;
}
