#include <iostream>
#include <bitset>
#include <vector>
// #include <fstream>
using namespace std;

int main(void) {
  // ofstream cout;
  // cout.open("answers.txt");
  int one_time, repeating;
  int test = 1;
  while(cin >> one_time >> repeating) {
    // cout << "Hi." << endl;
    if(one_time == 0 && repeating == 0)
      break;

    bitset<1000001> minutes(0);
    bitset<1000001> edge(0);
    vector<int> tracking;
    int a;
    int lower;
    int upper;
    int repeat;
    bool conflict = 0;
    tracking.clear();
    for(a = 0; a < 1000001; a++) {
      tracking.push_back(0);
    }
    for(a = 0; a < one_time; a++) { //This takes care of the non repeating tasks
      cin >> lower >> upper;

      // this checks if the edges of the range are actually occupied with tasks or are they just edges
      if((minutes[lower] == true && edge[lower] == false) || (minutes[upper] == true && edge[upper] == false)) {
        conflict = true;
      }
      if(upper - lower == 1 && tracking[lower] == upper)
        conflict = true;
      // if the test passes it goes in this else condition
      else if(conflict == false)  {
        if(upper - lower == 1)
          tracking[lower] = upper;

        int b;
        //update edge bitset
        (edge[lower] == 1) ? edge.flip(lower) : edge.set(lower);
        (edge[upper] == 1) ? edge.flip(upper) : edge.set(upper);
        minutes.set(lower);
        minutes.set(upper);
        //update minutes bistet
        for(b = lower + 1; b < upper && conflict == false; b++) {
          if(minutes[b] == true) {
            conflict = true;
          }
          else {
            minutes.set(b);
          }
        }
      }
    }

    for(a = 0; a < repeating; a++) {

      // cout << "Waiting for input" << endl;
      cin >> lower >> upper >> repeat;

      int lowerbase;
      int diff = upper - lower;
      int where_i_am = 1;

      for(lowerbase = lower; lowerbase < 1000000 && conflict == false; lowerbase += repeat) {
        // cout << lowerbase << endl;

        int upperbase = lowerbase + diff;
        if(upperbase > 1000000)
          upperbase = 1000000;
        if((minutes[lowerbase] == 1 && edge[lowerbase] == 0) || (minutes[upperbase] == 1 && edge[upperbase] == 0)) {
          conflict = true;
          break;
        }
        if(diff == 1 && tracking[lowerbase] == upperbase) {
          conflict = true;
          break;
        }
        if(conflict == false) {
          if(diff == 1)
            tracking[lowerbase] = upperbase;

          (edge[lowerbase] == 1) ? edge.flip(lowerbase) : edge.set(lowerbase);
          (edge[upperbase] == 1) ? edge.flip(upperbase) : edge.set(upperbase);
          minutes.set(upperbase);
          minutes.set(lowerbase);

          for(where_i_am = 1; where_i_am < diff; where_i_am++) {
            if(lowerbase + where_i_am >= 1000000)
              break;
            else {
              if(minutes[lowerbase + where_i_am] == true ) {
                conflict = true;
                break;
              }
              else
                minutes.set(lowerbase + where_i_am);
            }
          }
          if(lowerbase + where_i_am >= 1000000)
            break;
        }
      }
      // cout << "lolwhat" << endl;
    }
    if(conflict == true)
      cout << "CONFLICT" << endl;
    else
      cout << "NO CONFLICT" << endl;

    // cout << minutes << endl;
  }
  // cout.close();
  return 0;
}
