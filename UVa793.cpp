#include <iostream>
#include <vector>
#include <set>
#include <string>
#include <sstream>
using namespace std;

int main(void) {

  int testcases = 0;
  scanf("%d\n", &testcases);

  int a;
  for(a = 0; a < testcases; a++) {
    if(a != 0 )
      cout << endl;

    int correct = 0;
    int incorrect = 0;
    long long unsigned num_of_comp;
    vector<set<long long unsigned> > vec_of_set;
    scanf("%lld\n", &num_of_comp);
    string input;
    while(getline(cin, input) && input.size() != 0) {

      stringstream ss;
      ss << input;

      char first;
      long long unsigned second;
      long long unsigned third;

      ss >> first >> second >> third;
      int b;
      if(first == 'c') {

        bool first_present = false;
        bool second_present = false;
        int first_where = 0;
        int second_where = 0;

        for(b = 0; b < vec_of_set.size(); b++) {

          if((first_present == false) && (vec_of_set[b].find(second) != vec_of_set[b].end())) {
            first_present = true;
            first_where = b;
          }
          if((second_present == false) && (vec_of_set[b].find(third) != vec_of_set[b].end())) {
            second_present = true;
            second_where = b;
          }
          if(first_present == true && second_present == true)
            break;

        }
        if(first_present == false && second_present == false) {
          set<long long unsigned> place_me;
          place_me.insert(second);
          place_me.insert(third);

          vec_of_set.push_back(place_me);
        }
        else if(first_present == true && second_present == false) {
          vec_of_set[first_where].insert(third);
        }

        else if(first_present == false && second_present == true) {
          vec_of_set[second_where].insert(second);
        }

        else if((first_present == true && second_present == true) && (first_where != second_where)) {
          if(first_where < second_where) {
            vec_of_set[first_where].insert(vec_of_set[second_where].begin(), vec_of_set[second_where].end());
            vec_of_set.erase(vec_of_set.begin() + second_where);
          }
          else {
            vec_of_set[second_where].insert(vec_of_set[first_where].begin(), vec_of_set[first_where].end());
            vec_of_set.erase(vec_of_set.begin() + first_where);
          }
        }
      }
      else if(first == 'q') {
        if(second == third) {
          correct += 1;
          continue;
        }
        else {
          bool first_present = false;
          bool second_present = false;
          for(b = 0; b < vec_of_set.size(); b++) {
            if(vec_of_set[b].find(second) != vec_of_set[b].end()) {
              first_present = true;
              if(vec_of_set[b].find(third) != vec_of_set[b].end()) {
                correct += 1;
                break;
              }
              else {
                incorrect += 1;
                break;
              }
            }

            if(vec_of_set[b].find(third) != vec_of_set[b].end()) {
              second_present = true;
              if(vec_of_set[b].find(second) != vec_of_set[b].end()) {
                correct += 1;
                break;
              }
              else {
                incorrect += 1;
                break;
              }
            }
          }

          if(first_present == false && second_present == false)
            incorrect += 1;
        }

      }

    }

    printf("%d,%d\n", correct, incorrect);
  }
  return 0;
}
