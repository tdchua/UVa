#include <iostream>
#include <string>
using namespace std;

int main(void) {

  int num_of_words;
  cin >> num_of_words;

  int a;
  for(a = 0; a < num_of_words; a++) {

    string input;
    cin >> input;

    int b;
    int one_error = 0;
    int two_error = 0;
    int three_error = 0;
    string one = "one";
    string two = "two";
    for(b = 0; b < 3; b++) {

      if(input[b] != one[b])
        one_error += 1;
      if(input[b] != two[b])
        two_error += 1;

      if(one_error > 1 && two_error > 1) {
        break;
      }
    }

    if(one_error <= 1)
      printf("1\n");
    else if(two_error <= 1)
      printf("2\n");
    else
      printf("3\n");
  }
  return 0;
}
