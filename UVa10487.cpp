#include <iostream>
#include <stdlib.h>
#include <limits>


using namespace std;

int inf = numeric_limits<int>::max();

int main( void ) {

  int n;
  int counter = 1;
  while( cin >> n ) {

    if( n == 0 )
      break;
    cout << "Case " << counter << ":" << endl;
    int arrayofint[n];

    int a;
    for( a = 0; a < n; a++ ) {
      cin >> arrayofint[a];
    }

    int querries;
    cin >> querries;

    for( a = 0; a < querries; a++ ) {
      int querry;
      int diff;
      int sum;
      int diffprime = inf;
      int sumprime = 0;
      cin >> querry;
      for( int b = 0; b < n; b++ ) {
        for( int c = 0; c < n; c++ ) {
          if( b == c )
            continue;
          else {
            sum = arrayofint[b] + arrayofint[c];
            diff = abs( querry - sum );
            if( diff < diffprime ) {
              diffprime = diff;
              sumprime = sum;
            }
          }
        }
      }
      cout << "Closest sum to " << querry << " is " << sumprime << "." << endl;
    }
    counter++;
  }
  return 0;
}
