#include <iostream>
#include <stack>
#include <vector>
#include <queue>
#include <string>
using namespace std;

int main(void) {

  int number_of_coaches;

  while(cin >> number_of_coaches) {
    int a;

    if(number_of_coaches == 0)
      break;
    //the order of the incoming cars
    vector<int> incoming;
    for(a = 1; a <= number_of_coaches; a++)
      incoming.push_back(a);
    bool end = false;
    while(end == false) {

      //to gather the permutation of the coaches required
      queue<int> requirement;
      int coach_number;
      bool possible;
      for(a = 0; a < number_of_coaches; a++) {
        cin >> coach_number;
        if(coach_number == 0) {
          end = true;
          break;
        }
        requirement.push(coach_number);
      }

      if(end == true)
        break;

      a = 0; //incoming iterator
      int b = 0; //requirement iterator
      stack<int> coaches_in_station;
      possible = true;


      while(!requirement.empty()) {
        // cout << "Hi." << endl;
        if(coaches_in_station.empty() == false) {
          if(b < number_of_coaches && requirement.front() > coaches_in_station.top()) {
            coaches_in_station.push(incoming[b]);
            b = b + 1;
          }
        }
        else if(coaches_in_station.empty() == true) {
          coaches_in_station.push(incoming[b]);
          b = b + 1;
        }

        // cout << coaches_in_station.top() << " " << requirement.front() << endl;
        if(coaches_in_station.top() == requirement.front()) {
          requirement.pop();
          coaches_in_station.pop();
        }

        if(coaches_in_station.empty() == false && requirement.empty() == false) {
          if(coaches_in_station.top() > requirement.front()) {
            possible = false;
            break;
          }
        }


      }
      string answer;
      (possible == true) ? answer = "Yes" : answer = "No";
      cout << answer << endl;
    }
    cout << endl;

  }
  return 0;
}
