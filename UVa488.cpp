#include <iostream>

using namespace std;

int main( void ) {

  int testcasenumber;

  int ampli;
  int freq;
  int howmany;
  scanf( "%d", &testcasenumber );

  for( howmany = 0; howmany < testcasenumber; howmany++ ) {

    scanf( "%d", &ampli );
    scanf( "%d", &freq );

    int i;

    for( i = 0; i < freq; i++ ) { //Freq

      int j;

      if( i != 0 )
        cout << endl;

      int end = 0;

      for( j = 1; j <= ampli; ) { //Ampli

        int k;
        for( k = 1; k <= j; k++ ) { //Each unit in Ampli

          cout << j;

        }
        cout << endl;
        if( ( j == 1 && end == 1 ) || ampli == 1 )
          break;
        if( j == ampli)
          end = 1;
        if( end == 0 )
          j++;
        if( end == 1 && j != 1 )
          j--;

      }
    }

    if( howmany != testcasenumber - 1 )
      cout << endl;

  }

  return 0;
}
