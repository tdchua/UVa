import sys, os


if __name__ == '__main__' :

    listoffib = []
    i = 0
    firstnum = 0
    secondnum = 1
    sumoffirstsecond = 0
    fibnum = 5

    while ( i <= 5000 ) :
        listoffib.append( firstnum )
        sumoffirstsecond = firstnum + secondnum
        firstnum = secondnum
        secondnum = sumoffirstsecond
        i = i + 1
    try:
        while int(fibnum) >= 0 :
            try:
                fibnum = input( "" )
                print( "The Fibonacci number for", int(fibnum), "is", listoffib[int( fibnum )] )
            except EOFError:
                break
    except:
        sys.exit( os.EX_CONFIG )
