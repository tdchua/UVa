#include <iostream>

using namespace std;


int main(void) {

  int numberofstacks;
  int setnumber = 1;

  while( cin >> numberofstacks ) {

    int arrayofnumbers[numberofstacks];
    int i = 0;
    int average = 0;
    int stackstomove = 0;
    int sum = 0;


    if( numberofstacks == 0 )
      break;

    for( i = 0; i < numberofstacks; i++ ) {
      scanf("%d", &arrayofnumbers[i]);
      sum = sum + arrayofnumbers[i];
    }

    average = sum / numberofstacks;

    for( i = 0; i < numberofstacks; i++ ) {
      if( arrayofnumbers[i] > average )
        stackstomove = stackstomove + ( arrayofnumbers[i] - average );
    }

    cout << "Set #" << setnumber << endl;
    cout << "The minimum number of moves is " << stackstomove << '.' << endl;
    cout << endl;
    setnumber = setnumber + 1;


  }
  return 0;
}
