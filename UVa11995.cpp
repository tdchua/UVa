#include <iostream>
#include <queue>
#include <stack>

using namespace std;

int main(void) {

  int n;
  while(cin >> n) {

    int a;
    int command, element;
    stack<int> my_stack;
    queue<int> my_queue;
    priority_queue<int> my_pqueue;
    int my_stack_true = 1;
    int my_queue_true = 1;
    int my_pqueue_true = 1;

    for(a = 0; a < n; a++) {

      scanf("%d %d", &command, &element);
      if(command == 1) {
        my_stack.push(element);
        my_queue.push(element);
        my_pqueue.push(element);
      }

      else {
        if(my_stack.empty() == true) {
          my_stack_true = 0;
        }

        if(my_stack.empty() == false) {
          if(element != my_stack.top())
            my_stack_true = 0;

          if(element == my_stack.top() && my_stack_true == 1)
            my_stack.pop();
        }

        if(my_queue.empty() == true) {
          my_queue_true = 0;
        }

        if(my_queue.empty() == false) {
          if(element != my_queue.front())
            my_queue_true = 0;

          if(element == my_queue.front() && my_queue_true == 1)
            my_queue.pop();
        }


        if(my_pqueue.empty() == true) {
          my_pqueue_true = 0;
        }
        if(my_pqueue.empty() == false) {
          if(element != my_pqueue.top())
            my_pqueue_true = 0;

          if(element == my_pqueue.top() && my_pqueue_true == 1)
            my_pqueue.pop();
        }



      }


    }
    int sum_of_answers = my_pqueue_true + my_queue_true + my_stack_true;

    if(sum_of_answers == 0)
      printf("impossible\n");

    else if(sum_of_answers > 1)
      printf("not sure\n");

    else if(sum_of_answers == 1) {
      if(my_pqueue_true == 1)
        printf("priority queue\n");
      else if(my_stack_true == 1)
        printf("stack\n");
      else
        printf("queue\n");
    }

  }
  return 0;
}
