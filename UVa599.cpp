#include <iostream>
#include <set>
#include <string>
#include <vector>
using namespace std;

int main(void) {

  int testcases;
  cin >> testcases;

  int a;
  for(a = 0; a < testcases; a++) {

    string input;
    vector<set<char> > graph;
    int b;
    while(cin >> input) {
      // cout << "First Char: " << first_char << endl;

      if(input[0] == '*') {
        break;
      }

      char elem1, elem2;
      elem1 = input[1];
      elem2 = input[3];
      // printf("%c %c\n", elem1, elem2);
      if(elem1 == elem2)
        continue;

      bool elem1_present = false;
      bool elem2_present = false;
      int elem1_where = 0;
      int elem2_where = 0;

      for(b = 0; b < graph.size(); b++) {
        if(graph[b].find(elem1) != graph[b].end()) {
          elem1_present = true;
          elem1_where = b;
        }
        if(graph[b].find(elem2) != graph[b].end()) {
          elem2_present = true;
          elem2_where = b;
        }

      }

      if(elem1_present == false && elem2_present == false) {
        set<char> new_set;
        new_set.insert(elem1);
        new_set.insert(elem2);
        graph.push_back(new_set);
      }

      else if(elem1_present == true && elem2_present == false) {
        graph[elem1_where].insert(elem2);
      }

      else if(elem1_present == false && elem2_present == true) {
        graph[elem2_where].insert(elem1);
      }

      else if(elem1_present == true && elem2_present == true && elem1_where != elem2_where) {

        graph[elem1_where].insert(graph[elem2_where].begin(), graph[elem2_where].end());
        graph.erase(graph.begin() + elem2_where);

      }
    }

    string list_of_char;
    cin >> list_of_char;

    int acorns = 0;
    int trees = 0;
    bool tree_or_not[graph.size()];
    for(b = 0; b < graph.size(); b++) {
      tree_or_not[b] = false;
    }
    for(b = 0; b < list_of_char.size(); b += 2){
      int c;
      bool found_it = false;
      for(c = 0; c < graph.size(); c++) {
        if(graph[c].find(list_of_char[b]) != graph[c].end()) {
          tree_or_not[c] = true;
          found_it = true;
          break;
        }
      }
      (found_it) ? acorns += 0 : acorns += 1;
    }

    for(b = 0; b < graph.size(); b++) {
      if(tree_or_not[b] == true)
        trees += 1;
    }

    printf("There are %d tree(s) and %d acorn(s).\n", trees, acorns);
    getline(cin, list_of_char);
  }
  return 0;
}
