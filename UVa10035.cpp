#include <iostream>
using namespace std;

long long int a,b;
int i;
int carryamount;
long long int sum;
int carryover = 10;
int carryyes = 0;
long long int holderA = 0;
long long int holderB = 0;
long long int swaphold = 0;
//We assume that A would always hold the larger value.

int main( void ) {

  while ( cin >> a >> b ) {

    carryamount = 0;
    carryyes = 0;
    if ( a == 0 && b == 0 )
      break;
    if ( b > a ) {
      swaphold = a;
      a = b;
      b = swaphold;
    }

    for ( i = 0 ; i <= 10 ; i ++ ) {

      holderA = a % 10;
      holderB = b % 10;
      if ( carryyes == 1 ) {
        holderA = holderA + 1;
        carryyes = 0;
      }

      sum = holderA + holderB;
      if ( sum >= 10 ) {
        carryyes = 1;
        carryamount = carryamount + 1;
      }
      a = a / 10;
      b = b / 10;
    }
    if ( carryamount == 0 )
      cout << "No carry operation." << endl;
    else if ( carryamount == 1 )
      cout << "1 carry operation." << endl;
    else
      cout << carryamount << " carry operations." << endl;

  }
}
