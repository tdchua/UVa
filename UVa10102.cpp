#include <iostream>
#include <utility>
#include <vector>
#include <math.h>

using namespace std;

int main(void) {

  int M;
  while(cin >> M) {

    char gameboard[M][M];

    int a;
    vector<pair<int, int> > for_1;
    vector<pair<int, int> > for_3;
    pair<int, int> coordinates;

    for(a = 0; a < M; a++) {
      int b;
      for(b = 0; b < M; b++) {
        cin >> gameboard[a][b];
        if(gameboard[a][b] == '3') {
          coordinates.first = b;
          coordinates.second = a;
          for_3.push_back(coordinates);
        }
        else if(gameboard[a][b] == '1') {
          coordinates.first = b;
          coordinates.second = a;
          for_1.push_back(coordinates);
        }
      }
    }

    int min_all = 0;

    for(a = 0; a < for_1.size(); a++) {
      int b;
      int min = 0;
      for(b = 0; b < for_3.size(); b++) {
        int diff = abs(for_1[a].first - for_3[b].first) + abs(for_1[a].second - for_3[b].second);
        if(diff < min || min == 0) {
          min = diff;
        }
      }
      if(min > min_all || min_all == 0) {
        min_all = min;
      }
    }
    printf("%d\n", min_all);


  }
  return 0;
}
