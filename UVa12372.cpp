#include <iostream>
using namespace std;

int main(void) {

  int testcases;
  cin >> testcases;

  for(int a = 0; a < testcases; a++) {

    int length, width, height;
    cin >> length >> width >> height;

    if(length > 20 || width > 20 || height > 20)
      printf("Case %d: bad\n", a + 1);
    else
      printf("Case %d: good\n", a + 1);


  }
  return 0;
}
