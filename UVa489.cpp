#include <iostream>
#include <string>

using namespace std;

int main( void ) {

  int roundnumber;

  while( cin >> roundnumber ) {

    if( roundnumber == -1 )
      break;

    string answer;
    string guess;
    bool same = false;

    cin >> answer;
    cin >> guess;

    int a = 0;
    int b = 0;
    bool present = false;

    int correctamount = 0;
    int wrongamount = 0;
    cout << "Round " << roundnumber << endl;
    for( a = 0; a < guess.length(); a++ ) {
      same = false;
      for ( b = 0; b < a; b++ ) {
        if( guess[a] == guess[b] )
          same = true;
      }
      if( same == false ) {
        present = false;
        for ( b = 0; b < answer.length(); b++ ) {
          if( guess[a] == answer[b] ) {
            correctamount++;
            present = true;
          }
        }
        if( present == false ) {
          wrongamount++;
        }
      }
      if( wrongamount > 6 ) {
        cout << "You lose." << endl;
        break;
      }
      if( correctamount == answer.length() ) {
        cout << "You win." << endl;
        break;
      }
    }
    if( wrongamount < 7 && correctamount < answer.length() )
      cout << "You chickened out." << endl;
  }
  return 0;
}
