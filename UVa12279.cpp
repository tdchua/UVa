#include <iostream>
using namespace std;
int main(void) {

  int events;
  int testcase = 1;
  while(cin >> events) {

    if(events == 0)
      break;

    int a;
    int num_of_treat = 0;
    int num_of_reason = 0;
    int input;

    for(a = 0; a < events; a++) {
      cin >> input;
      if(input != 0)
        num_of_reason += 1;
      else
        num_of_treat += 1;

    }
    int answer = num_of_reason - num_of_treat;

    printf("Case %d: %d\n", testcase, answer);
    testcase++;
  }
  return 0;
}
