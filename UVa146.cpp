#include <iostream>
#include <algorithm>
#include <vector>
#include <string>
using namespace std;

int main( void ) {

  string input;
  while( cin >> input ) {

    if( input == "#" )
      break;

    if( next_permutation( input.begin(), input.end() ) ) {
      cout << input << endl;
    }
    else
      cout << "No Successor" << endl;

  }

  return 0;
}
