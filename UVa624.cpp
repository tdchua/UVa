#include <iostream>
#include <vector>
using namespace std;

int remaining;
vector<int> answer;

void traverse(vector<int> &vec_of_int, int N, vector<int> traversed, int a) {

  for(; a < vec_of_int.size(); a++) {

    int number_taken;
    number_taken = vec_of_int[a];

    if(N - number_taken >= 0) {
      int b;

      traversed.push_back(number_taken);

      traverse(vec_of_int, N - number_taken, traversed, a + 1);

      if(!traversed.empty()) {
        traversed.pop_back();
      }

    }

  }
  int b;
  if(remaining == -1 || N < remaining) {
    remaining = N;
    answer = traversed;
  }
}

int main(void) {

  int N;
  while(cin >> N) {

    int num_of_tracks;
    cin >> num_of_tracks;

    int a;
    vector<int> pool;
    for(a = 0; a < num_of_tracks; a++) {
      int minutes;
      cin >> minutes;
      pool.push_back(minutes);
    }
    vector<int> filler;
    remaining = -1;
    traverse(pool, N, filler, 0);
    int sum = 0;
    for(a = 0; a < answer.size(); a++) {
      sum += answer[a];
      printf("%d ", answer[a]);
    }
    printf("sum:%d\n", sum);
  }
  return 0;
}
