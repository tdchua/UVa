#include <iostream>
using namespace std;

int main( void ) {

  int big, small;
  while( cin >> big >> small ) {
    if( big == small && big == 0 )
      break;

    char bigorigmatrix[big][big];
    char smallorigmatrix[small][small];
    char small90degmatrix[small][small];
    char small180degmatrix[small][small];
    char small270degmatrix[small][small];

    int a = 0;
    int b = 0;
    for( a = big - 1; a >= 0 ; a-- )
      for( b = 0; b < big; b++ )
        cin >> bigorigmatrix[a][b];


    for( a = small-1; a >= 0 ; a-- )
      for( b = 0; b < small; b++ )
        small90degmatrix[a][b] = '0';
    for( a = small-1; a >= 0 ; a-- )
      for( b = 0; b < small; b++ )
        small180degmatrix[a][b] = '0';
    for( a = small-1; a >= 0 ; a-- )
      for( b = 0; b < small; b++ )
        small270degmatrix[a][b] = '0';

    for( a = small-1; a >= 0 ; a-- )
      for( b = 0; b < small; b++ )
        cin >> smallorigmatrix[a][b];


    bool even = false;
    if( small % 2 == 0 ) {
      even = true;
    }

    int numberofsquares = small / 2;
    int row;
    int column;
    int numberoftimes = small;

    for( a = 0; a < numberofsquares; a++ ) {
      row = small - 1 - a;
      column = 0 + a;
      b = row; // 1
      int c = column; // 0
      int i;

      for( i = 0; i < numberoftimes; i++ ) {
        // 4 x 4 matrix (3,3) -> (2,2)
        small90degmatrix[b-i][b] = smallorigmatrix[b][c+i];
        small180degmatrix[c][b-i] = smallorigmatrix[b][c+i];
        small270degmatrix[c+i][c] = smallorigmatrix[b][c+i];
      }


      b = row;
      c = row;
      // cout << "B: " << b << " C: " << c;
      for( i = 0; i < numberoftimes; i++ ) {
        small90degmatrix[a][b-i] = smallorigmatrix[b-i][c];
        small180degmatrix[a+i][a] = smallorigmatrix[b-i][c];
        small270degmatrix[b][a+i] = smallorigmatrix[b-i][c];

      }
      b = row;
      c = row;
      for( i = 0; i < numberoftimes; i++ ) {
        small90degmatrix[a+i][a] = smallorigmatrix[a][b-i];
        small180degmatrix[b][a+i] = smallorigmatrix[a][b-i];
        small270degmatrix[b-i][c] = smallorigmatrix[a][b-i];

      }
      for( i = 0; i < numberoftimes; i++ ) {
        small90degmatrix[b][a+i] = smallorigmatrix[a+i][a];
        small180degmatrix[b-i][c] = smallorigmatrix[a+i][a];
        small270degmatrix[a][b-i] = smallorigmatrix[a+i][a];

      }

      numberoftimes = numberoftimes - 2;
    }
    if( even == false ) {
      small90degmatrix[numberofsquares][numberofsquares] = smallorigmatrix[numberofsquares][numberofsquares];
      small180degmatrix[numberofsquares][numberofsquares] = smallorigmatrix[numberofsquares][numberofsquares];
      small270degmatrix[numberofsquares][numberofsquares] = smallorigmatrix[numberofsquares][numberofsquares];
    }

    // cout << "Orig" << endl;
    // for( a = small-1; a >= 0; a-- ) {
    //   for( b = 0; b < small; b++ )
    //     cout << smallorigmatrix[a][b] << " ";
    //   cout << endl;
    // }
    //
    // cout << "90 Deg" << endl;
    // for( a = small-1; a >= 0; a-- ) {
    //   for( b = 0; b < small; b++ )
    //     cout << small90degmatrix[a][b] << " ";
    //   cout << endl;
    // }
    //
    // cout << "180 Deg" << endl;
    // for( a = small-1; a >= 0; a-- ) {
    //   for( b = 0; b < small; b++ )
    //     cout << small180degmatrix[a][b] << " ";
    //   cout << endl;
    // }
    // cout << "270 Deg" << endl;
    // for( a = small-1; a >= 0; a-- ) {
    //   for( b = 0; b < small; b++ )
    //     cout << small270degmatrix[a][b] << " ";
    //   cout << endl;
    // }

    // cout << "180 Deg" << endl;
    // for( a = small-1; a >= 0; a-- ) {
    //   for( b = 0; b < small; b++ )
    //     cout << small180degmatrix[a][b] << " ";
    //   cout << endl;
    // }

    int norot = 0;
    int ninetyrot = 0;
    int oneeightyrot = 0;
    int twoseventyrot = 0;
    int d;
    int e;
    for( a = big - 1; a + 1 - small >= 0 ; a-- ) {
      for( b = 0; b + small <= big; b++ ) {
        d = a;
        e = b;
        int g;
        int h;
        bool yessame1 = true;
        bool yessame2 = true;
        bool yessame3 = true;
        bool yessame4 = true;
        for( g = small-1; g >= 0; g--, d-- ) {
          e = b;
          for( h = 0; h < small; h++, e++ ) {
            // cout << a << " " << b << endl;
            // cout << bigorigmatrix[d][e] << endl;

            if( bigorigmatrix[d][e] != smallorigmatrix[g][h] )
              yessame1 = false;
            if( bigorigmatrix[d][e] != small90degmatrix[g][h] )
              yessame2 = false;
            if( bigorigmatrix[d][e] != small180degmatrix[g][h] )
              yessame3 = false;
            if( bigorigmatrix[d][e] != small270degmatrix[g][h] )
              yessame4 = false;
          }
        }
        norot = norot + yessame1;
        ninetyrot = ninetyrot + yessame2;
        oneeightyrot = oneeightyrot + yessame3;
        twoseventyrot = twoseventyrot + yessame4;
      }
    }
    cout << norot << " " << ninetyrot << " " << oneeightyrot << " " << twoseventyrot << endl;
  }
  return 0;
}




// cout << "Orig" << endl;
// for( a = small-1; a >= 0; a-- ) {
//   for( b = 0; b < small; b++ )
//     cout << smallorigmatrix[a][b] << " ";
//   cout << endl;
// }
//
// cout << "90 Deg" << endl;
// for( a = small-1; a >= 0; a-- ) {
//   for( b = 0; b < small; b++ )
//     cout << small90degmatrix[a][b] << " ";
//   cout << endl;
// }
//
// cout << "180 Deg" << endl;
// for( a = small-1; a >= 0; a-- ) {
//   for( b = 0; b < small; b++ )
//     cout << small180degmatrix[a][b] << " ";
//   cout << endl;
// }
// cout << "270 Deg" << endl;
// for( a = small-1; a >= 0; a-- ) {
//   for( b = 0; b < small; b++ )
//     cout << small270degmatrix[a][b] << " ";
//   cout << endl;
// }
