#include <iostream>

using namespace std;


int main( void ) {

  string input;

  while( cin >> input ) {

      char chessboard[8][8];
      int a,b;

      int c = 7;
      int d = 0;
      for( a = 0; a < 8; a++ )
        for( b = 0; b < 8; b++ )
          chessboard[a][b] ='e';

      for( a = 0; a < input.length(); a++ ) {

        int numbermaybe = input[a] - '0';
        if( numbermaybe <= 9 && numbermaybe >= 0 )
          for( b = 0; b < numbermaybe; b++ ) {
            chessboard[c][d] = 'e';
            d++;
          }
        else if( input[a] != '/' ) {
          chessboard[c][d] = input[a];
          d++;
        }
        else if( input[a] == '/' ) {
          d = 0;
          c--;
        }
      }

      for( a = 7; a >= 0; a-- ) {
        for( b = 0; b < 8; b++ ) {

          if( chessboard[a][b] == 'k' || chessboard[a][b] == 'K' ) {
            if( a + 1 < 8 )
              if( chessboard[a+1][b] == 'e' )
                chessboard[a+1][b] = 'x';
            if( a + 1 < 8 && b + 1 < 8 )
              if( chessboard[a+1][b+1] == 'e' )
                chessboard[a+1][b+1] = 'x';
            if( b + 1 < 8 )
              if( chessboard[a][b+1] == 'e' )
                chessboard[a][b+1] = 'x';
            if( a - 1 >= 0 && b + 1 < 8 )
              if( chessboard[a-1][b+1] == 'e' )
                chessboard[a-1][b+1] = 'x';
            if( a - 1 >= 0 )
              if( chessboard[a-1][b] == 'e' )
                chessboard[a-1][b] = 'x';
            if( a - 1 >= 0 && b - 1 >= 0 )
              if( chessboard[a-1][b-1] == 'e' )
                chessboard[a-1][b-1] = 'x';
            if( b - 1 >= 0 )
              if( chessboard[a][b-1] == 'e' )
                chessboard[a][b-1] = 'x';
            if( b - 1 >= 0 && a + 1 < 8 )
              if( chessboard[a+1][b-1] == 'e' )
                chessboard[a+1][b-1] = 'x';

          }

          if( chessboard[a][b] == 'q' || chessboard[a][b] == 'Q' ) {
            c = a;
            d = b;
            for( ; a + 1 < 8; a++ )
              if( chessboard[a+1][b] == 'e' )
                chessboard[a+1][b] = 'x';
              else if( chessboard[a+1][b] != 'x')
                break;
            a = c;
            b = d;
            while( a + 1 < 8 && b + 1 < 8 ) {
              if( chessboard[a+1][b+1] == 'e' )
                chessboard[a+1][b+1] = 'x';
              else if( chessboard[a+1][b+1] != 'x')
                break;
              a++;
              b++;
            }
            a = c;
            b = d;
            for( ; b + 1 < 8; b++ )
              if( chessboard[a][b+1] == 'e' )
                chessboard[a][b+1] = 'x';
              else if( chessboard[a][b+1] != 'x' )
                break;
            a = c;
            b = d;
            while( a - 1 >= 0 && b + 1 < 8 ) {
              if( chessboard[a-1][b+1] == 'e' )
                chessboard[a-1][b+1] = 'x';
              else if( chessboard[a-1][b+1] != 'x')
                break;
              a--;
              b++;
            }
            a = c;
            b = d;
            for( ; a - 1 >= 0; a-- )
              if( chessboard[a-1][b] == 'e' )
                chessboard[a-1][b] = 'x';
              else if( chessboard[a-1][b] != 'x' )
                break;
            a = c;
            b = d;
            while( a - 1 >= 0 && b - 1 >= 0 ) {
              if( chessboard[a-1][b-1] == 'e' )
                chessboard[a-1][b-1] = 'x';
              else if( chessboard[a-1][b-1] != 'x' )
                break;
              a--;
              b--;
            }
            a = c;
            b = d;
            for( ; b - 1 >= 0; b-- )
              if( chessboard[a][b-1] == 'e' )
                chessboard[a][b-1] = 'x';
              else if( chessboard[a][b-1] != 'x' )
                break;
            a = c;
            b = d;
            while( a + 1 < 8 && b - 1 >= 0 ) {
              if( chessboard[a+1][b-1] == 'e' )
                chessboard[a+1][b-1] = 'x';
              else if( chessboard[a+1][b-1] != 'x')
                break;
              a++;
              b--;
            }
            a = c;
            b = d;
          }

          if( chessboard[a][b] == 'b' || chessboard[a][b] == 'B' ) {
            c = a;
            d = b;
            while( a + 1 < 8 && b + 1 < 8 ) {
              if( chessboard[a+1][b+1] == 'e' )
                chessboard[a+1][b+1] = 'x';
              else if( chessboard[a+1][b+1] != 'x' )
                break;
              a++;
              b++;
            }
            a = c;
            b = d;
            while( a - 1 >= 0 && b + 1 < 8 ) {
              if( chessboard[a-1][b+1] == 'e' )
                chessboard[a-1][b+1] = 'x';
              else if( chessboard[a-1][b+1] != 'x' )
                break;
              a--;
              b++;
            }
            a = c;
            b = d;
            while( a - 1 >= 0 && b - 1 >= 0 ) {
              if( chessboard[a-1][b-1] == 'e' )
                chessboard[a-1][b-1] = 'x';
              else if( chessboard[a-1][b-1] != 'x')
                break;
              a--;
              b--;
            }
            a = c;
            b = d;
            while( a + 1 < 8 && b - 1 >= 0 ) {
              if( chessboard[a+1][b-1] == 'e' )
                chessboard[a+1][b-1] = 'x';
              else if( chessboard[a+1][b-1] != 'x' )
                break;
              a++;
              b--;
            }
            a = c;
            b = d;
          }

          if( chessboard[a][b] == 'r' || chessboard[a][b] == 'R' ) {
            c = a;
            d = b;
            for( ; a + 1 < 8; a++ )
              if( chessboard[a+1][b] == 'e' )
                chessboard[a+1][b] = 'x';
              else if( chessboard[a+1][b] != 'x' )
                break;
            a = c;
            b = d;
            for( ; b + 1 < 8; b++ )
              if( chessboard[a][b+1] == 'e' )
                chessboard[a][b+1] = 'x';
              else if( chessboard[a][b+1] != 'x' )
                break;
            a = c;
            b = d;
            for( ; a - 1 >= 0; a-- )
              if( chessboard[a-1][b] == 'e' )
                chessboard[a-1][b] = 'x';
              else if( chessboard[a-1][b] != 'x' )
                break;
            a = c;
            b = d;
            for( ; b - 1 >= 0; b-- )
              if( chessboard[a][b-1] == 'e' )
                chessboard[a][b-1] = 'x';
              else if( chessboard[a][b-1] != 'x' )
                break;
            a = c;
            b = d;
          }

          if( chessboard[a][b] == 'n' || chessboard[a][b] == 'N') {

            if( a + 2 < 8 && b + 1 < 8 )
              if( chessboard[a+2][b+1] == 'e' )
                chessboard[a+2][b+1] = 'x';
            if( a + 2 < 8 && b - 1 >= 0 )
              if( chessboard[a+2][b-1] == 'e' )
                chessboard[a+2][b-1] = 'x';
            if( a + 1 < 8 && b + 2 < 8 )
              if( chessboard[a+1][b+2] == 'e' )
                chessboard[a+1][b+2] = 'x';
            if( a - 1 >= 0 && b + 2 < 8 )
              if( chessboard[a-1][b+2] == 'e' )
                chessboard[a-1][b+2] = 'x';
            if( a - 2 < 8 && b + 1 < 8 )
              if( chessboard[a-2][b+1] == 'e' )
                chessboard[a-2][b+1] = 'x';
            if( a - 2 < 8 && b - 1 >= 0 )
              if( chessboard[a-2][b-1] == 'e' )
                chessboard[a-2][b-1] = 'x';
            if( a + 1 < 8 && b - 2 >= 0 )
              if( chessboard[a+1][b-2] == 'e' )
                chessboard[a+1][b-2] = 'x';
            if( a - 1 >= 0 && b - 2 >= 0 )
              if( chessboard[a-1][b-2] == 'e' )
                chessboard[a-1][b-2] = 'x';
          }

          if( chessboard[a][b] == 'P' ) { //white
            if( a + 1 < 8 && b + 1 < 8 )
              if( chessboard[a+1][b+1] == 'e' )
                chessboard[a+1][b+1] = 'x';
            if( a + 1 < 8 && b - 1 >= 0 )
              if( chessboard[a+1][b-1] == 'e' )
                chessboard[a+1][b-1] = 'x';
          }

          if( chessboard[a][b] == 'p' ) { //black
            if( a - 1 >= 0 && b + 1 < 8 )
              if( chessboard[a-1][b+1] == 'e' )
                chessboard[a-1][b+1] = 'x';
            if( a - 1 >= 0 && b - 1 >= 0 )
              if( chessboard[a-1][b-1] == 'e' )
                chessboard[a-1][b-1] = 'x';
          }

          // for( c = 7; c >= 0; c-- ) {
          //   for( d = 0; d < 8; d++ )
          //     cout << chessboard[c][d];
          //   cout << endl;
          // }
          // cout << a << " " << b << endl;

        }
      }
      int answer = 0;

      for( a = 7; a >= 0; a-- )
        for( b = 0; b < 8; b++ )
          if( chessboard[a][b] == 'e' )
            answer++;

      cout << answer << endl;
      // for( a = 7; a >= 0; a-- ) {
      //   for( b = 0; b < 8; b++ )
      //     cout << chessboard[a][b];
      //   cout << endl;
      // }
  }
  return 0;
}
