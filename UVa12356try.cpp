#include <iostream>
#include <set>
// #include <iterator>
using namespace std;

int main( void ) {

  int numberofsoldiers;
  int numberofreports;

  while( cin >> numberofsoldiers >> numberofreports ) {

    if( numberofsoldiers == numberofreports && numberofsoldiers == 0 )
      break;

      set<int> battalion;
      set<int>::iterator leftpointer;
      set<int>::iterator rightpointer;

      int a;
      for( a = 1; a < numberofsoldiers + 1; a++ ) {
        battalion.insert(a);
      }

      int killleft;
      int killright;

      // for( leftpointer = battalion.begin(); leftpointer != battalion.end(); ++leftpointer ) {
      //   cout << *leftpointer << " ";
      // }
      // cout << endl;

      for( a = 0; a < numberofreports; a++ ) {
        cin >> killleft >> killright;
        // leftpointer = --battalion.lower_bound(killleft);
        // rightpointer = ++battalion.lower_bound(killright);
        // cout << *leftpointer << " " << *rightpointer << endl;

        leftpointer = battalion.lower_bound(killleft);
        rightpointer = ++battalion.lower_bound(killright);

        battalion.erase( leftpointer, rightpointer );

        if( battalion.empty() == true ) {
          cout << "* *" << endl;
          continue;
        }
        // for( leftpointer = battalion.begin(); leftpointer != battalion.end(); ++leftpointer ) {
        //   cout << *leftpointer << " ";
        // }
        // cout << endl;

        if( battalion.upper_bound( killright ) == battalion.begin() ) {
          cout << "* ";
        }
        else {
          leftpointer = --battalion.lower_bound(killleft);
          cout << *leftpointer << " ";
        }

        if( battalion.lower_bound( killleft ) == battalion.end() )
          cout << "*" << endl;
        else {  
          rightpointer = battalion.upper_bound(killright);
          cout << *rightpointer << endl;
        }
      }
      cout << "-" << endl;
  }
  return 0;
}
