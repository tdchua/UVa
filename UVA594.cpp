#include <iostream>
#include <bitset>
using namespace std;

int main( void ) {
  int a;
  while( cin >> a ){
    bitset<32> b1 = a;
    // cout << b1 << endl;
    bitset<32> answer;

    int i = 0;
    int j;
    for( i = 0, j = 3; i < 4; i++ ) {
      int k = 7;
      for( k = 7; k >= 0; k-- ) {
        answer[(j * 8) + k ] = b1[(i * 8) + k];
      }
      j--;
    }
    int answertoint = answer.to_ulong();
    cout << a << " converts to " << answertoint << endl;
  }
  return 0;
}
