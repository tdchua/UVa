#include <iostream>

using namespace std;

int main(void) {

  int testcasenumber;
  int peoplenumber;


  while( scanf( "%d", &testcasenumber ) == 1 ) {
    int i;
    for( i = 0; i < testcasenumber; i++ ) {

      scanf("%d", &peoplenumber);
      int arrayofscores[peoplenumber];
      int sum = 0;
      int average = 0;
      int j = 0;

      for( j = 0; j < peoplenumber; j++ ) {

        scanf("%d", &arrayofscores[j] );
        sum = sum + arrayofscores[j];

      }

      average = sum / peoplenumber;

      int abovecount = 0;

      for( j = 0; j < peoplenumber; j++ ) {
        if( arrayofscores[j] > average )
          abovecount++;
      }

      float answer;

      answer = (float) abovecount / (float) peoplenumber;

      printf("%.3f\%%\n", answer * 100);
    }
  }
  return 0;
}
