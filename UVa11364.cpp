#include <iostream>
#include <algorithm>
#include <vector>
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
using namespace std;

int main(void) {

  int testcases;
  cin >> testcases;

  int a;
  for(a = 0; a < testcases; a++) {
    int shops_num;
    cin >> shops_num;
    int b;
    int sum = 0;
    vector<int> shops;
    int shop_coor;
    for(b = 0; b < shops_num; b++) {
      cin >> shop_coor;
      sum += shop_coor;
      shops.push_back(shop_coor);
    }
    sort(shops.begin(), shops.end());
    float average = sum / shops_num;
    average = round(average);

    int diff = 0;
    diff += abs(shops[0] - average);
    for(b = 1; b < shops_num; b++) {
      diff += shops[b] - shops[b - 1];
    }
    diff += shops[shops_num - 1] - average;

    printf("%d\n", diff);
  }
  return 0;
}
