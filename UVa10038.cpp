#include <iostream>
#include <string>
#include <cctype>

using namespace std;

int numberseq[3010];
int input = 0;
int main() {

  while (scanf("%d", &input) == 1) {
    int currentnumber = 0;
    int pastnumber = 0;
    int goalscore = 0;
    int j = 0;
    int i = 0;

    for( j = 0 ; j < 3010 ; j++ ) {
      numberseq[j] = -1;
    }

    for( j = 1 ; j < input ; j++ ) {

      numberseq[j] = j;

    }

    int numberofcorrects = 0;
    int finaldecision = 1;
    int dumbsum = 0;
    int dummy = 0;
    int absdifference = 0;

    scanf("%d", &currentnumber);

    for( i = 1 ; i < input ; i++ ) {
      pastnumber = currentnumber;
      scanf("%d", &currentnumber);
      absdifference = abs(pastnumber - currentnumber);
      if( absdifference == numberseq[absdifference] ) {
        numberseq[absdifference] = -1;
        numberofcorrects++;
      }
      else {
        finaldecision = 0;
      }

    }

    if( finaldecision == 1 && numberofcorrects == input-1 ) {
      cout << "Jolly" << endl;
    }
    else {
      cout << "Not jolly" << endl;
    }
  }
}
