#include <iostream>
#include <algorithm>
#include <vector>

using namespace std;

int main( void ) {

  unsigned int input;
  vector<unsigned int> timvec;

  while( cin >> input ) {

    timvec.push_back(input);
    sort( timvec.begin(), timvec.end() );
    if( timvec.size() % 2 == 1 ) {
      cout << timvec[timvec.size() / 2 ] << endl;

    }
    else {
      unsigned int half = timvec.size() / 2;
      unsigned int lol = ( timvec[half] + timvec[half-1] ) / 2;
      cout << lol << endl;
    }
  }
  return 0;
}
