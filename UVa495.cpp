#include <iostream>

using namespace std;


int main( void ) {

  unsigned long long int arrayoffib[5000] = { 0 };
  unsigned long long int firstnum = 0;
  unsigned long long int secondnum = 1;
  unsigned long long int sum;
  int input;
  int count = 0;
  int i;

  for( i = 0; i < 5000; i++ ) {

    arrayoffib[i] = firstnum;
    sum = firstnum + secondnum;
    firstnum = secondnum;
    secondnum = sum;

  }

  while( cin >> input) {

    cout << "The Fibonacci number for " << input << " is " << arrayoffib[input] << endl ;

  }

  return 0;
}
