
#include <iostream>

using namespace std;

int testcases = 0;
int input[3];
int i = 0;
int casecount = 1;
int answer = 0;
int holder = 0;

int main(void) {

  while( scanf("%d", &testcases) == 1 ) {
    casecount = 1;
    while( casecount <= testcases ) {

      int max = 1000;
      int min = 10000;
      for( i = 0 ; i < 3 ; i++ ) {
        scanf("%d", &input[i]);
      }

      for( i = 0 ; i < 3 ; i++ ) {

        if ( max <= input[i] )
          max = input[i];
        if ( min >= input[i] )
          min = input[i];

      }

      for( i = 0 ; i < 3 ; i++ ) {
        if ( input[i] != max && input[i] != min )
          answer = input[i];
      }

      cout << "Case " << casecount << ": " << answer << endl;
      casecount = casecount + 1;
    }
  }
}
