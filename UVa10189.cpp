#include <iostream>
#include <string>

using namespace std;


int main(void) {

  int i,j;
  int A,B;
  int fieldnumber = 1;
  while( cin >> A >> B ) {

    if( A == 0 && B == 0 )
      break;

    char inmatrix[A][B];
    char outmatrix[A][B];
    char input;
    // I = RowNumber
    // J = ColNumber

    for( i = 0 ; i < A ; i++ ) {
      for( j = 0 ; j < B ; ) {
        scanf("%c",&input);
        if( input != '\n' ) {

            inmatrix[i][j] = input;
            j++;

        }

      }
    }

    for( i = 0 ; i < A ; i++ ) {
      for( j = 0 ; j < B ; j++ ) {
        if( inmatrix[i][j] != '*')
          outmatrix[i][j] = '0';
        else
          outmatrix[i][j] = inmatrix[i][j];
      }
    }


    for( i = 0 ; i < A ; i++ ) {
      for( j = 0 ; j < B ; j++ ) {

        if( outmatrix[i][j] == '*') {

          if( i != 0 && outmatrix[i-1][j] != '*' ) // left
            outmatrix[i-1][j] = outmatrix[i-1][j] + 1;

          if( j != 0 && outmatrix[i][j-1] != '*' ) // up
            outmatrix[i][j-1] = outmatrix[i][j-1] + 1;

          if( i != 0 && j != 0 && outmatrix[i-1][j-1] != '*' ) // upper left
            outmatrix[i-1][j-1] = outmatrix[i-1][j-1] + 1;

          if( i != A-1 && j != 0 && outmatrix[i+1][j-1] != '*'  ) // lower left
            outmatrix[i+1][j-1] = outmatrix[i+1][j-1] + 1;

          if( i != A-1 && outmatrix[i+1][j] != '*'  ) // down
            outmatrix[i+1][j] = outmatrix[i+1][j] + 1;

          if( i != 0 && j != B-1 && outmatrix[i-1][j+1] != '*' ) //upper right
            outmatrix[i-1][j+1] = outmatrix[i-1][j+1] + 1;

          if( j != B-1 && outmatrix[i][j+1] != '*' ) // right
            outmatrix[i][j+1] = outmatrix[i][j+1] + 1;

          if( i != A-1 && j != B-1 && outmatrix[i+1][j+1] != '*' ) // lower right
            outmatrix[i+1][j+1] = outmatrix[i+1][j+1] + 1;
        }
      }
    }
    if( fieldnumber > 1 )
      cout << endl;
      
    cout << "Field #" << fieldnumber << ':' << endl;
    fieldnumber = fieldnumber + 1;
    for( i = 0 ; i < A ; i++ ) {
      for( j = 0 ; j < B ; j++ ) {

          cout << outmatrix[i][j];

      }
      cout << endl;
    }
  }
}
