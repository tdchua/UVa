#include <iostream>

using namespace std;


int f91( int N ) {

  if( N <= 100 )
    return f91( f91( N + 11 ));

  else
    return N - 10;

}




int main( void ) {

  int input;

  while( scanf( "%d", &input ) == 1 ) {

    if( input == 0 )
      break;

    else 
      cout << "f91(" << input << ") = " << f91( input ) << endl;

  }
  return 0;
}
