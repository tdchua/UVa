#include <iostream>
// #include <fstream>

using namespace std;

bool yesornoleap( int year ) {
  if(( year % 4 == 0 ) && ( year % 400 == 0 || year % 100 != 0 )) {
    return true;
  }
  else
    return false;
}

int main( void ) {

  // ofstream cout;
  // cout.open( "UVaAnswers.txt");
  unsigned int dayspermonth[13] = { 0, 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31 };
  unsigned int leapdayspermonth[13] = { 0, 31, 29, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31 };
  unsigned int tousedayspermonth[13] = { 0, 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31 };
  int datasets,i;

  scanf( "%d", &datasets );

  for( i = 0; i < datasets; i++ ) {

    int datemenstrualcycle;
    scanf( "%d", &datemenstrualcycle );
    //MMDDYYYY
    int month = datemenstrualcycle / 1000000;
    int date = ( datemenstrualcycle / 10000 ) % 100;
    int year = ( datemenstrualcycle % 10000 );
    int x;

    // cout << month << ' ' <<  date << ' ' << year << ' ' << endl;
    if( yesornoleap(year) == true ) {
      for( x = 0; x < 13; x++ )
        tousedayspermonth[x] = leapdayspermonth[x];
    }
    else
      for( x = 0; x < 13; x++ )
        tousedayspermonth[x] = dayspermonth[x];


    int monthcounter = 1;
    int b = 280;
    b = b + date;

    int a;
    for( a = month; b != 0; a++ ) {

      if( b > tousedayspermonth[a] ) {
        b = b - tousedayspermonth[a];
        if( a == 12 ) {
          a = 0;
          year = year + 1;
          if( yesornoleap( year )) {
            for( x = 0; x < 13; x++ )
              tousedayspermonth[x] = leapdayspermonth[x];
          }
          else
            for( x = 0; x < 13; x++ )
              tousedayspermonth[x] = dayspermonth[x];
        }
      }
      else {
        date = b;
        month = a;
        break;
      }

    }
    // for( x = 280; x != 0; ) {
    //
    //   if( monthcounter == month ) {
    //     x--;
    //     if( date != tousedayspermonth[month] )
    //       date = date + 1;
    //     else {
    //       x++;
    //       date = 0;
    //       month++;
    //       monthcounter++;
    //
    //       if( month == 12 ) {
    //         month = 1;
    //         monthcounter = 1;
    //         year++;
    //         int j;
    //         if( yesornoleap(year) == true ) {
    //           for( j = 0; j < 13; j++ )
    //             tousedayspermonth[j] = leapdayspermonth[j];
    //         }
    //         else
    //           for( j = 0; j < 13; j++ )
    //             tousedayspermonth[j] = dayspermonth[j];
    //
    //         while( x != 0 ) {
    //           if( x > tousedayspermonth[monthcounter] ) {
    //             x = x - tousedayspermonth[monthcounter];
    //             monthcounter++;
    //           }
    //           else {
    //             month = monthcounter;
    //             date = x;
    //             x = 0;
    //           }
    //         }
    //       }
    //     }
    //   }
    // }

     cout << i + 1 << ' ';

    if( month < 10 )
      cout << '0' << month << '/';
    if( month >= 10 )
      cout << month << '/';
    if( date < 10 )
      cout << '0' << date << '/';
    if( date >= 10 )
      cout << date << '/';
    if( year == 0 )
      cout << "0000";
    else if( year < 10 )
      cout << "000" << year;
    else if( year < 100 )
      cout << "00" << year;
    else if( year < 1000 )
      cout << "0" << year;
    else if( year >= 1000 )
      cout << year;

    int sumofmonthdate = ( month * 100 ) + date;
    cout << ' ';
    if( 121 <= sumofmonthdate && sumofmonthdate <= 219 )
      cout << "aquarius" << endl;
    else if( 220 <= sumofmonthdate && sumofmonthdate <= 320 )
      cout << "pisces" << endl;
    else if( 321 <= sumofmonthdate && sumofmonthdate <= 420 )
      cout << "aries" << endl;
    else if( 421 <= sumofmonthdate && sumofmonthdate <= 521 )
      cout << "taurus" << endl;
    else if( 522 <= sumofmonthdate && sumofmonthdate <= 621 )
      cout << "gemini" << endl;
    else if( 622 <= sumofmonthdate && sumofmonthdate <= 722 )
      cout << "cancer" << endl;
    else if( 723 <= sumofmonthdate && sumofmonthdate <= 821 )
      cout << "leo" << endl;
    else if( 822 <= sumofmonthdate && sumofmonthdate <= 923 )
      cout << "virgo" << endl;
    else if( 924 <= sumofmonthdate && sumofmonthdate <= 1023 )
      cout << "libra" << endl;
    else if( 1024 <= sumofmonthdate && sumofmonthdate <= 1122 )
      cout << "scorpio" << endl;
    else if( 1123 <= sumofmonthdate && sumofmonthdate <= 1222 )
      cout << "sagittarius" << endl;
    else if( 1223 <= sumofmonthdate || sumofmonthdate <= 120 )
      cout << "capricorn" << endl;

  }

  return 0;
}
