#include <iostream>
#include <math.h>

using namespace std;

int main(void) {

 double power,exponent,base;

  while( scanf("%lf%lf", &power, &exponent) == 2) {

    double i;
    double answer = 0;
    double ten;

    // cout << exponent << endl;
    answer = pow( exponent, 1/power );
    printf("%.0lf\n", answer);

  }
  return 0;
}
