#include <iostream>
#include <bitset>
#include <math.h>
using namespace std;

int main(void) {

  int N;
  while(cin >> N) {

    int exponent = pow(2, N);
    int i;
    int weights[exponent];
    int potency[exponent];

    for(i = 0; i < exponent; i++)
      cin >> weights[i];

    int sum;
    int j;
    int bitset_to_int;
    for(i = 0; i < exponent; i++) {
      bitset<15> coordinates(i);
      sum = 0;
      for(j = 0; j < N; j++) {
        bitset<15> neighbor = coordinates;
        neighbor.flip(j);
        bitset_to_int = (int)(neighbor.to_ulong());
        sum = sum + weights[bitset_to_int];
        neighbor.flip(j);
      }
      potency[i] = sum;
    }
    int maxsum = 0;
    for(i = 0; i < exponent; i++) {
      bitset<15> coordinates(i);
      for(j = 0; j < N; j++ ) {
        bitset<15> neighbor = coordinates;
        neighbor.flip(j);
        bitset_to_int = (int)(neighbor.to_ulong());
        sum = potency[i] + potency[bitset_to_int];
        if(maxsum < sum)
          maxsum = sum;
        neighbor.flip(j);
      }
    }
    cout << maxsum << endl;
  }
  return 0;
}
