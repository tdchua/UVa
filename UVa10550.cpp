#include <iostream>
#include <stdio.h>
#include <stdlib.h>
using namespace std;

int main(void) {

  int init_pos, first, second, third;
  while(cin >> init_pos >> first >> second >> third) {

    if(init_pos == 0 && first == 0 && second == 0 && third == 0)
      break;

    int answer = 0;
    answer += 720;

    int init_to_first = 0;

    //CCW movement
    if(init_pos < first)
      answer += 360 - (abs(first - init_pos) * 9);
    else if(init_pos > first)
      answer += abs(first - init_pos) * 9;
    // cout << answer - 720 << endl;

    answer += 360;
    //CW movement
    if(first < second) {
      answer += abs(second - first) * 9;
    }
    else {
      answer += 360 - (abs(second - first) * 9);
      // cout << abs(second - first) * 9 << endl;
    }

    //CCW movement
    if(second < third)
      answer += 360 - (abs(third - second) * 9);
    else if(second > third)
      answer += abs(third - second) * 9;

    cout << answer << endl;
  }
  return 0;
}


//for counterclockwise rotation
//the hand goes clockwise
/*
  two inputs a and b
  a is initial position
  if a > b
  360 - a + b
  if a < b
  a - b



*/
