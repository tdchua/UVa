#include <iostream>
using namespace std;

int main( void ) {

  int testcase = 0;
  cin >> testcase;

  int a;

  for( a = 0; a < testcase; a++ ) {

    int bingoboard[5][5];
    int b;
    int c;

    for( b = 4; b >= 0; b-- )
      for( c = 0; c < 5; c++ ) {
        if( b == 2 && c == 2 )
          bingoboard[b][c] = 0;
        else
          cin >> bingoboard[b][c];
      }
    // for( b = 4; b >= 0; b-- ) {
    //   for( c = 0; c < 5; c++ )
    //     cout << bingoboard[b][c] << " ";
    //   cout << endl;
    // }
    int d;
    int callednumber;
    int winningturn = 0;
    bool winornowin = false;

    for( d = 1; d <= 75 && winornowin == false; d++ ) {
      cin >> callednumber;
      for( b = 0; b < 5; b++ )
        for( c = 0; c < 5; c++ )
          if( bingoboard[b][c] == callednumber )
            bingoboard[b][c] = 0;
      if( d >= 4 ) {
        //check columns first
        int streak = 0;
        for( c = 0; c < 5; c++ ) {
          for( b = 0; b < 5; b++ ) {
            if( bingoboard[b][c] == 0 )
              streak++;
          }
          if( streak == 5 ) {
            winningturn = d;
            winornowin = true;
            break;
          }
          streak = 0;
        }
        streak = 0;
        for( b = 0; b < 5; b++ ) {
          for( c = 0; c < 5; c++ ) {
            if( bingoboard[b][c] == 0 )
              streak++;
          }
          if( streak == 5 ) {
            winningturn = d;
            winornowin = true;
            break;
          }
          streak = 0;
        }
        streak = 0;
        int e;
        for( e = 0; e < 5; e++ ) {
          b = e;
          c = e;
          if( bingoboard[b][c] == 0 )
            streak++;
        }
        if( streak == 5 ) {
          winningturn = d;
          winornowin = true;
          break;
        }
        b = 4;
        c = 0;
        streak = 0;
        for( e = 0; e < 5; e++ ) {
          if( bingoboard[b][c] == 0 )
            streak++;
          b--;
          c++;
        }
        if( streak == 5 ) {
          winningturn = d;
          winornowin = true;
          break;
        }
      }
    }
    int buffer = 0;
    for( d = winningturn; d < 75; d++ )
      cin >> buffer;

    // cout << endl;
    // for( b = 4; b >= 0; b-- ) {
    //   for( c = 0; c < 5; c++ )
    //     cout << bingoboard[b][c] << " ";
    //   cout << endl;
    // }
    cout << "BINGO after " << winningturn << " numbers announced" << endl;
  }
  return 0;
}
