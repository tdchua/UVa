#include <iostream>
#include <string>
#include <vector>
#include <ctype.h>
#include <algorithm>
using namespace std;

bool timcompare( char a, char b ) {
  if( islower(a) != 0 && islower(b) != 0 )  { // A and B
    return a < b;
  }
  else if( islower(a) == 0 && islower(b) == 0 ) { // a and b
    return a < b;
  }
  else if( islower(a) != 0 && islower(b) == 0 ) { // a and B
    if( toupper(a) == b )
      return false;
    else
      return toupper(a) < b;
    
  }
  else if( islower(a) == 0 && islower(b) != 0 ) { // A and b
    if( a == toupper(b) )
      return true;
    else
      return a < toupper(b);
    // b -= ('a'-'A');
    // return a <= b;
  }
  else
    return a < b;
}

int main( void ) {

  int testcases;
  int a;
  cin >> testcases;
  for( a = 0; a < testcases; a++ ) {
    string input;
    cin >> input;
    sort( input.begin(), input.end(), timcompare );
    do {
        cout << input << '\n';
    } while( next_permutation( input.begin(), input.end(), timcompare ));
  }
  return 0;
}
