#include <iostream>
#include <set>

using namespace std;


int main(void) {

  int N;
  int M;

  while(scanf("%d %d", &N, &M)) {

    if(N == 0 && M == 0)
      break;

    int a;
    int Jack_CD;
    set<int> Jack;

    for(a = 0; a < N; a++) {
      cin >> Jack_CD;
      Jack.insert(Jack_CD);
    }

    int same_count = 0;
    int Jill_CD;
    for(a = 0; a < M; a++) {

      cin >> Jill_CD;

      if(Jack.count(Jill_CD) == 1)
        same_count += 1;
    }

    printf("%d\n", same_count);


  }
  return 0;
}
