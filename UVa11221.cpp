#include <iostream>
#include <string>
#include <math.h>

using namespace std;

int main( void ) {

  int testcases;
  char buffer;
  cin >> testcases;
  scanf("%c", &buffer);
  int a;
  for( a = 0; a < testcases; a++ ) {

    string input;
    getline( cin, input );

    int b;
    for( b = 0; b < input.length(); b++ ) {
      if( input[b] == '.' || input[b] == ',' || input[b] == '!' || input[b] == '?' || input[b] == ' ' || input[b] == '(' || input[b] == ')' ) {
        input.erase(b,1);
        b = -1;
      }
    }
    // cout << input.length() << endl;
    int toroot = sqrt(input.length());

    if( toroot * toroot != input.length() ) {
      cout << "Case #" << a + 1 << ":" << endl;
      cout << "No magic :(" << endl;
    }

    else {

      int c;
      char palindromeboard[toroot][toroot];
      int e = 0;

      for( b = toroot - 1; b >= 0; b-- )
        for( c = 0; c < toroot; c++ ) {
          palindromeboard[b][c] = input[e];
          e++;
        }


      bool palindboard = true;
      b = toroot - 1;
      c = 0;
      int d = 0;
      for( d = 0; d < toroot - 1; d++ ) {
        if( palindromeboard[b][c+d] != palindromeboard[b-d][c] ) {
          palindboard = false;
          break;
        }
        if( palindromeboard[b][c+d] != palindromeboard[c+d][b] )  {
          palindboard = false;
          break;
        }
        if( palindromeboard[b][c+d] != palindromeboard[c+d][b] ) {
          palindboard = false;
          break;
        }
        if( palindromeboard[b][c+d] != palindromeboard[c][b-d ] ) {
          palindboard = false;
          break;
        }

      }

      cout << "Case #" << a + 1 << ":" << endl;
      if( palindboard == false ) {
        cout << "No magic :(" << endl;
      }
      else {
        cout << toroot << endl;
      }
    }
  }
  return 0;
}
