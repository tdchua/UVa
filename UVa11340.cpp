#include <iostream>
using namespace std;

int main( void ) {

  int testcases;
  cin >> testcases;
  int a;
  for( a = 0; a < testcases; a++ ) {
    int b,c;
    cin >> b;
    char letters[b];
    int cents[b];

    for( c = 0; c < b; c++ ) {
      cin >> letters[c];
      cin >> cents[c];
    }

    int lines;
    cin >> lines;

    char input;
    scanf( "%c", &input );

    int e;
    int total = 0;

    for( e = 0; e < lines;  ) {
      scanf( "%c", &input );
      if( input == '\n' )
        e++;
      else {
        for( c = 0; c < b; c++ ) {
          if( input == letters[c] )
            total = total + cents[c];
        }
      }
    }

    // cout << total << endl;

    int remainderafterdivide;
    remainderafterdivide = total % 100;
    // cout << total << endl;
    if( remainderafterdivide < 10 )
      cout << total / 100 << ".0" << remainderafterdivide << "$" << endl;
    else
      cout << total / 100 << "." << remainderafterdivide << "$" << endl;
  }
  return 0;
}
