#include <iostream>

using namespace std;

char turnface( char face ) {

  if( face == 'E')
    return 'N';
  else if( face == 'N' )
    return 'W';
  else if( face == 'W' )
    return 'S';
  else
    return 'E';

}


struct coordinates {
  int x;
  int y;
};

int digits( int number ) {
  if( number > 99 )
    return 3;
  else if( number > 9 )
    return 2;
  else
    return 1;
}

int main( void ) {

  int rows, columns;
  while( cin >> rows >> columns ) {

    if( rows == 0 & columns == 0 )
      return 0;

    int a;
    int b;
    char face;
    char setup[rows][columns];
    bool turn = false;

    for( a = rows - 1 ; a >= 0; a-- ) {
      for( b = 0; b < columns; b++ ) {
        scanf( "%c", &setup[a][b] );
        if( setup[a][b] == '\n' )
          b = b - 1;
        if( setup[a][b] == '1' )
          setup[a][b] = 'x';
      }
    }

    // for( a = rows - 1; a >= 0; a-- ) {
    //   for( b = 0; b < columns; b++ ) {
    //     cout << setup[a][b];
    //   }
    //   cout << endl;
    // }

    coordinates location;
    location.x = 0; //columns
    location.y = 0; //rows

    coordinates start;
    start = location;

    face = 'E';

    //move one block right
    if( columns > 1 ) {
      if( setup[location.y][location.x + 1] != 'x' ) {
        setup[location.y][location.x]++;
        location.x++;
      }
    }
    //move one block up
    else {
      setup[location.y][location.x]++;
      location.y++;
      face = 'N';
    }


    while( 1 ) {

      // cout << "Face: " << face << " Location" << " X : " << location.x << " Y : " << location.y  << endl;
      if( location.x == start.x && location.y == start.y )
        break;

      switch( face ) {

        case 'N':
          if( location.y + 1 < rows ) {
            if( setup[location.y + 1][location.x] != 'x' ) {
              if( location.x + 1 < columns )
                if( setup[location.y][location.x + 1 ] == 'x' && setup[location.y + 1 ][location.x + 1 ] != 'x' )
                  face = 'E';
              setup[location.y][location.x]++;
              location.y++;
            }
            else
              face = turnface(face);
          }
          else
            face = turnface(face);
        break;

        case 'S':
          if( location.y - 1 >= 0 ) {
            if( setup[location.y - 1][location.x] != 'x' ) {
              if( location.x - 1 >= 0 )
                if( setup[location.y][location.x - 1] == 'x' && setup[location.y - 1][location.x - 1] != 'x' )
                  face = 'W';
              setup[location.y][location.x]++;
              location.y--;
            }
            else
              face = turnface(face);
          }
          else
            face = turnface(face);
        break;

        case 'W':
          if( location.x - 1 >= 0 ) {
            if( setup[location.y][location.x - 1] != 'x' ) {
              if( location.y + 1 < rows )
                if( setup[location.y + 1][location.x] == 'x' && setup[location.y + 1 ][location.x - 1] != 'x' )
                  face = 'N';
              setup[location.y][location.x]++;
              location.x--;
            }
            else
              face = turnface(face);
          }
          else
            face = turnface(face);
        break;

        case 'E':
          if( location.x + 1 < columns ) {
            if( setup[location.y][location.x + 1] != 'x' ) {
              if( location.y - 1 >= 0 )
                if( setup[location.y - 1][location.x] == 'x' && setup[location.y - 1 ][location.x + 1] != 'x' )
                  face = 'S';
              setup[location.y][location.x]++;
              location.x++;
            }
            else
              face = turnface(face);
          }
          else
            face = turnface(face);
        break;
      }


    }

    // cout << endl << endl;
    // cout << "NEW BOARD: " << endl;
    // for( a = rows - 1; a >= 0; a-- ) {
    //   for( b = 0; b < columns; b++ ) {
    //     cout << setup[a][b];
    //   }
    //   cout << endl;
    // }

    int never = 0;
    int once = 0;
    int twice = 0;
    int thrice = 0;
    int four = 0;
    int fifth = 0;

    for( a = rows - 1; a >= 0; a-- ) {
      for( b = 0; b < columns; b++ )
        if( setup[a][b] == '0' )
          never++;
        else if( setup[a][b] == '1' )
          once++;
        else if( setup[a][b] == '2' )
          twice++;
        else if( setup[a][b] == '3' )
          thrice++;
        else if( setup[a][b] == '4' )
          four++;
      }

    // 3 = 0
    // 2 = 1
    // 1 = 2
    for( a = 0; a < 3 - digits(never); a++ )
      cout << ' ';
    cout << never;
    for( a = 0; a < 3 - digits(once); a++ )
      cout << ' ';
    cout << once;
    for( a = 0; a < 3 - digits(twice); a++ )
      cout << ' ';
    cout << twice;
    for( a = 0; a < 3 - digits(thrice); a++ )
      cout << ' ';
    cout << thrice;
    for( a = 0; a < 3 - digits(four); a++ )
      cout << ' ';
    cout << four << endl;

  }
  return 0;
}
