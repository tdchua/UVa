#include <iostream>
#include <string>


using namespace std;



bool palindromecheck( string input ) {

  int strlength;
  strlength = input.size();

  int i;
  int oddoreven;
  int palind = 1;


  for( i = 0 ; i < strlength / 2 ; i++ ) {

    if( input[i] != input[strlength - 1 - i ] )
      palind = 0;

  }

  if( palind == 1 )
    return true;
  else
    return false;

}

bool mirrorcheck( string input ) {

  int i;
  int strlength;
  int mirror;

  strlength = input.size();

  mirror = 1;
  for( i = 0; i < strlength ; i++ ) {

    if( input[i] == 'A' && input[strlength - 1 - i] != 'A' )
      mirror = 0;
    if( input[i] == 'E' && input[strlength - 1 - i] != '3' )
      mirror = 0;
    if( input[i] == 'H' && input[strlength - 1 - i] != 'H' )
      mirror = 0;
    if( input[i] == 'I' && input[strlength - 1 - i] != 'I' )
      mirror = 0;
    if( input[i] == 'J' && input[strlength - 1 - i] != 'L' )
      mirror = 0;
    if( input[i] == 'M' && input[strlength - 1 - i] != 'M' )
      mirror = 0;
    if( input[i] == 'O' && input[strlength - 1 - i] != 'O' )
      mirror = 0;
    if( input[i] == 'S' && input[strlength - 1 - i] != '2' )
      mirror = 0;
    if( input[i] == 'T' && input[strlength - 1 - i] != 'T' )
      mirror = 0;
    if( input[i] == 'U' && input[strlength - 1 - i] != 'U' )
      mirror = 0;
    if( input[i] == 'V' && input[strlength - 1 - i] != 'V' )
      mirror = 0;
    if( input[i] == 'W' && input[strlength - 1 - i] != 'W' )
      mirror = 0;
    if( input[i] == 'X' && input[strlength - 1 - i] != 'X' )
      mirror = 0;
    if( input[i] == 'Y' && input[strlength - 1 - i] != 'Y' )
      mirror = 0;
    if( input[i] == 'Z' && input[strlength - 1 - i] != '5' )
      mirror = 0;
    if( input[i] == '1' && input[strlength - 1 - i] != '1' )
      mirror = 0;
    if( input[i] == '2' && input[strlength - 1 - i] != 'S' )
      mirror = 0;
    if( input[i] == '3' && input[strlength - 1 - i] != 'E' )
      mirror = 0;
    if( input[i] == '5' && input[strlength - 1 - i] != 'Z' )
      mirror = 0;
    if( input[i] == '8' && input[strlength - 1 - i] != '8' )
      mirror = 0;
    if( input[i] == 'B' || input[i] == 'C' || input[i] == 'D' || input[i] == 'F' || input[i] == 'G' )
      mirror = 0;
    if( input[i] == 'K' || input[i] == 'N' || input[i] == 'P' || input[i] == 'Q' || input[i] == 'R' )
      mirror = 0;
    if( input[i] == '4' || input[i] == '7' || input[i] == '9' || input[i] == '6' || input[i] == 'B' )
      mirror = 0;

  }
  return mirror;
}








int main( void ) {

  string input;

  while( getline(cin, input) ) {

    if( mirrorcheck( input ) == true && palindromecheck( input ) == true )
      cout << input << " -- is a mirrored palindrome." << endl << endl;
    else if( mirrorcheck( input ) == true )
      cout << input << " -- is a mirrored string." << endl << endl;
    else if( palindromecheck( input ) == true )
      cout << input << " -- is a regular palindrome." << endl << endl;
    else
      cout << input << " -- is not a palindrome." << endl << endl;

  }
  return 0;
}
