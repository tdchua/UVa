#include <iostream>
#include <math.h>
#include <iomanip>
#include <cmath>

using namespace std;

int main( void ) {

  int noofstudents;

  while( scanf( "%d", &noofstudents ) ) {

    if( noofstudents == 0 )
      break;

    int i;
    int arrayofmoney[noofstudents];
    float floatinput;

    for( i = 0; i < noofstudents; i++ ) {

      scanf( "%f", &floatinput );
      floatinput = floatinput * 100.00;
      arrayofmoney[i] =  static_cast<int>(round(floatinput));
      // cout << arrayofmoney[i] << endl;

    }

    int average = 0;
    int centremaining = 0;

    for( i = 0; i < noofstudents; i++ )
      average = average + arrayofmoney[i];


    // cout << "Sum : " << average << "  ";

    centremaining = average % noofstudents;
    average = average / noofstudents;

    int highaverage = centremaining ? average + 1 : average ;


    int diff = 0;
    int morethanaveragecount = 0;
    int lessthanaveragecount = 0;
    int samewithaveragecount = 0;
    int butalsignal = 0;
    int negdiff = 0;
    // cout << "Average : " << average << "  Centremaining : " << centremaining;
    // cout << "HighAve : " << highaverage << endl;

    for( i = 0; i < noofstudents; i++ ) {

      if( arrayofmoney[i] > highaverage ) {
          diff = diff + arrayofmoney[i] - highaverage ;
      }

      if( arrayofmoney[i] < average ) {
          negdiff = negdiff + average - arrayofmoney[i];
      }

    }

    //  cout << "  Diff : " << diff << "  MoreThanAve : " << morethanaveragecount << "  LessThanAverage : " << lessthanaveragecount <<  "  NegDiff: " << negdiff << endl;

    int usedSum = max(diff, negdiff);
    cout << '$' << (usedSum / 100) << '.' << setw(2) << setfill('0') << (usedSum % 100) << '\n';


  }

  return 0;
}
