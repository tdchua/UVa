#include <iostream>

using namespace std;

int testcase;
int limit;
int i,j,k;
int arrayofcarriage[50];
int noofswaps;


int main(void) {

  while( scanf( "%d", &testcase) == 1 ) {

    for ( j = 0; j < testcase ; j++ ) {

      noofswaps = 0;
      scanf( "%d", &limit);

      for ( i = 0; i < limit ; i++ ) {
        scanf( "%d", &arrayofcarriage[i]);
      }
      for ( i = 0; i < limit ; i++ ) {

        if ( i + 1 < limit )
          if ( arrayofcarriage[i] > arrayofcarriage[i+1]) {

              swap( arrayofcarriage[i],arrayofcarriage[i+1] );
              noofswaps = noofswaps + 1;
              k = i;

              while ( arrayofcarriage[k] < arrayofcarriage[k-1] ) {

                swap( arrayofcarriage[k], arrayofcarriage[k-1] );
                if ( k - 1 != -1 )
                  k = k - 1;
                noofswaps = noofswaps + 1;
              }

          }
      }
      cout << "Optimal train swapping takes " << noofswaps << " swaps." << endl;

    }
  }

  return 0;
}
