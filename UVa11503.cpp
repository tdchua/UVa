#include <iostream>
#include <set>
#include <string>
#include <vector>
using namespace std;

int main(void) {

  int testcases;
  cin >> testcases;

  int a;
  for(a = 0; a < testcases; a++) {
    vector<set<string> > vec_of_friendship;
    int num_of_friendship;

    cin >> num_of_friendship;
    int b;
    for(b = 0; b < num_of_friendship; b++) {

      string first, second;
      cin >> first >> second;

      int c;
      bool present_first = false;
      bool present_second = false;
      int where_first = 0;
      int where_second = 0;

      for(c = 0; c < vec_of_friendship.size(); c++) {
        if(vec_of_friendship[c].find(first) != vec_of_friendship[c].end()) {
          present_first = true;
          where_first = c;
        }
        if(vec_of_friendship[c].find(second) != vec_of_friendship[c].end()) {
          present_second = true;
          where_second = c;
        }
      }

      if(present_first == false && present_second == false) {
        set<string> friendship;
        friendship.insert(first);
        friendship.insert(second);
        vec_of_friendship.push_back(friendship);
        printf("%lu\n", vec_of_friendship[c].size());
      }
      else if(present_first == true && present_second == false) {
        vec_of_friendship[where_first].insert(second);
        printf("%lu\n", vec_of_friendship[where_first].size());
      }
      else if(present_first == false && present_second == true) {
        vec_of_friendship[where_second].insert(first);
        printf("%lu\n", vec_of_friendship[where_second].size());
      }
      else {
        if(where_first == where_second)
          printf("%lu\n", vec_of_friendship[where_first].size());
        else {
          vec_of_friendship[where_first].insert(vec_of_friendship[where_second].begin(), vec_of_friendship[where_second].end());
          printf("%lu\n", vec_of_friendship[where_first].size());
          vec_of_friendship.erase(vec_of_friendship.begin() + where_second);
        }
      }
    }
  }
  return 0;
}
