#include <iostream>

using namespace std;

struct coordinate {

  int x;
  int y;

};

int main( void) {

  int testcase;

  while( scanf( "%d", &testcase ) ) {

    if( testcase == 0 )
      break;

    int i;
    struct coordinate divpoint;

    scanf( "%d", &divpoint.x );
    scanf( "%d", &divpoint.y );

    struct coordinate input;

    for( i = 0; i < testcase; i++ ) {

      scanf( "%d", &input.x );
      scanf( "%d", &input.y );

      if( input.x == divpoint.x || input.y == divpoint.y ) {
        cout << "divisa" << endl;
        continue;
      }

      if( ( input.x - divpoint.x > 0 ) && ( input.y - divpoint.y > 0 ) )
        cout << "NE" << endl;

      if( ( input.x - divpoint.x > 0 ) && ( input.y - divpoint.y < 0 ) )
        cout << "SE" << endl;

      if( ( input.x - divpoint.x < 0 ) && ( input.y - divpoint.y < 0 ) )
        cout << "SO" << endl;

      if( ( input.x - divpoint.x < 0 ) && ( input.y - divpoint.y > 0 ) )
        cout << "NO" << endl;

    }
  }
  return 0;
}
