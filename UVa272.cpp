#include <iostream>
#include <string>
#include <cctype>
using namespace std;
int main()
{
  string inputstr = "";
  string doublerightquote = "``";
  string doubleleftquote = "''";
  string singlerightquote = "`";
  string singleleftquote = "'";
  int pairnum = 0;

  while(getline (cin , inputstr)){

    if(cin.eof())
        return 0;

    for(int i=0; i<inputstr.size(); i++){

      if(inputstr[i] == '"' && pairnum == 0){
        inputstr.erase(i,1);
        inputstr.insert(i,doublerightquote);
        pairnum = 1;
      }
      else if(inputstr[i] == '"' && pairnum == 1){
        inputstr.erase(i,1);
        inputstr.insert(i,doubleleftquote);
        pairnum = 0;
      }

    }

    cout << inputstr << endl;

  }
  return 0;
}
