#include <iostream>

using namespace std;

int main( void ) {

  int rows, columns;
  while( cin >> rows >> columns ) {
    int numberofknights = 0;
    if( rows == 0 && columns == 0 )
      break;

    if( rows == 0 || columns == 0 ) {
      cout << "0 knights may be placed on a " << rows << " row " << columns << " column board." << endl;
    }
    else if( rows == 1 || columns == 1 )
      cout << rows*columns << " knights may be placed on a " << rows << " row " << columns << " column board." << endl;

    else if( rows == 2 || columns == 2 ) {
      int holder;
      if( rows != 2 )
        holder = rows;
      else
        holder = columns;

      char chessboard[holder];
      bool alternate = false;
      int twicecount = 0;
      int b,c;
      for( c = 0; c < holder; c++ )
        chessboard[c] = 'e';

      for( c = 0; c < holder; c++ ) {
        if( twicecount == 2 ) {
          twicecount = 0;
          alternate = !alternate;
        }
        if( alternate == false ) {
          chessboard[c] = 'x';
          twicecount++;
          numberofknights++;
        }
        if( alternate == true ) {
          chessboard[c] = 'e';
          twicecount++;
        }
      }

      cout << numberofknights*2 << " knights may be placed on a " << rows << " row " << columns << " column board." << endl;
    }

    else {

      int n, m;
      if( rows > columns ) {
        n = columns;
        m = rows;
      }
      else {
        n = rows; 
        m = columns;
      }

      numberofknights = (( n + 1 ) / 2) * (( m + 1 ) / 2 ) + ( n / 2 ) * ( m / 2 );

      cout << numberofknights << " knights may be placed on a " << rows << " row " << columns << " column board." << endl;

    }
  }
  return 0;
}
