#include <iostream>
#include <math.h>
using namespace std;


int main( void ) {

  unsigned int input;

  while( scanf( "%d", &input ) ) {

    if( input == 0 )
      break;
      
    unsigned int sqrtinput;
    sqrtinput = sqrt( input );

    if( sqrtinput * sqrtinput == input )
      cout << "yes" << endl;
    else
      cout << "no" << endl;


  }

  return 0;

}
