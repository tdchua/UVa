#include <iostream>
#include <string>

using namespace std;

int main( void ) {

  string input;
  int datasets;
  int score[2000] = { 0 };
  string country[2000];
  int countrycount = 0;
  int i;
  scanf( "%d", &datasets );
  while(getchar() != '\n');
  for( i = 0; i < datasets; i++ ) {

    int j = 0;
    char a;
    bool same = false;
    while( 1 ) {
      a = getchar();
      if( a != ' ' )
        break;
    }
    getline( cin, input );

    string delimiter = " ";
    string variable = a + input.substr( 0, input.find(delimiter));

    if( i == 0 ) {
      country[0] = variable;
      score[0] = 1;
    }

    for( j = 0; j <= countrycount; j++ ) {
      if( variable == country[j] && i!= 0 ) {
        score[j] = score[j] + 1;
        same = true;
        break;
      }
    }

    if( same == false && i!= 0 ) {
      countrycount = countrycount + 1;
      country[countrycount] = variable;
      score[countrycount] = 1;
    }

  }
  for( i = 0; i <= countrycount; i++ ) {
    int intholder;
    string stringholder;
    if( i + 1 <= countrycount ) {
      if( country[i] > country[i+1] ) {
        stringholder = country[i];
        country[i] = country[i+1];
        country[i+1] = stringholder;
        intholder = score[i];
        score[i] = score[i+1];
        score[i+1] = intholder;
        if( i - 1 >= 0 )
          i = i - 2;
      }
    }
  }
  for( i = 0; i <= countrycount; i++ ) {

    cout << country[i] << " " << score[i] << endl;
  }

  return 0;
}
