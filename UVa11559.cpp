#include <iostream>

using namespace std;

int main( void ) {

  int participants, budget, numhotels, weeks;

  while( cin >> participants >> budget >> numhotels >> weeks ) {

    int i;
    int price, availroom;
    int minprice = 10000 * participants;
    bool answer = false;

    for( i = 0; i < numhotels; i++ ) {

      scanf( "%d", &price );

      int j;
      for( j = 0; j < weeks; j++ ) {

        scanf( "%d", &availroom );
        if( price * participants <= budget && availroom >= participants && minprice > price * participants ) {

          minprice = price * participants;
          answer = true;

        }
      }
    }

    if( answer == false )
      cout << "stay home" << endl;
    else
      cout << minprice << endl;
  }
  return 0;
}
