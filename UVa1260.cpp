#include <iostream>
using namespace std;

int main( void ) {

  int testcases;
  cin >> testcases;

  for( int a = 0; a < testcases; a++ ) {

    int numberofnumbers;
    cin >> numberofnumbers;
    int A[numberofnumbers];

    for( int b = 0; b < numberofnumbers; b++ )
      cin >> A[b];

    int B[numberofnumbers-1];

    for( int b = 1; b < numberofnumbers; b++ ) {
      int c;
      int count = 0;
      for( c = b - 1; c >= 0; c-- ) {
        if( A[b] >= A[c] ) {
          count++;
        }
      }
      B[b-1] = count;
    }
    int sum = 0;
    for( int b = 0; b < numberofnumbers - 1; b++ ) {
      sum = sum + B[b];
    }
    cout << sum << endl;
  }
  return 0;
}
