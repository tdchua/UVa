#include <iostream>
#include <math.h>

using namespace std;

int main( void ) {

  int SZ;
  int P;

  while( cin >> SZ >> P ) {
    int line = 0;
    int column = 0;
    if( SZ == P && SZ == 0 )
      break;

    int a = 0;
    long double timnumber = 1;

    while( P > pow( timnumber, 2) ) {
      timnumber = timnumber + 2;
      a++;
    }

    long double upperlimit = pow( timnumber, 2 );
    long double lowerlimit;
    int b;
    for( b = 1; ; b++ ) {
      lowerlimit = upperlimit - timnumber + 1;
      if( lowerlimit <= P) {
        break;
      }
      upperlimit = lowerlimit;
    }
     // cout << pow( timnumber, 2 ) << endl;

    if( b == 1 ) {
      column = ( SZ / 2 ) + 1 + a;
      line = ( SZ / 2 ) + 1 + a - ( upperlimit - P );
    }
    else if( b == 2 ) {
      line = ( SZ / 2 ) + 1 - a;
      column = (SZ / 2 ) + 1  + a - ( upperlimit - P );
    }
    else if( b == 3 ) {
      line = ( SZ / 2 ) + 1 - a + ( upperlimit - P );
      column = ( SZ / 2 ) + 1 - a;
    }
    else {
      line = ( SZ / 2 ) + 1 + a;
      column = ( SZ / 2 ) + 1 - a + ( upperlimit - P );
    }

    cout << "Line = " << line << ", column = " << column << "." << endl;
  }

  return 0;
}
