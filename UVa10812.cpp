#include <iostream>

using namespace std;

int main( void ) {

  unsigned int datasets;
  cin >> datasets;

  int i;
  for( i = 0; i < datasets; i++ ) {

  unsigned int sum, diff;

  cin >> sum >> diff;

  if( diff > sum )
    cout << "impossible" << endl;

  else {
    unsigned int x;
    x = ( diff + sum ) / 2;
    if((( 2 * x ) - sum == diff ) && x >= 0 && x - diff >= 0 )
      cout << x << " " << x - diff << endl;
    else
      cout << "impossible" << endl;
  }

  // x + y = sum
  // x - y = |diff|
  // y = sum - x
  // x - sum + x = |diff|

  }
  return 0;
}
