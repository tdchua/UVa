#include <iostream>
#include <vector>
#include <string>
using namespace std;


struct stoplight {
  string state;
  int green;
  int red;
  int orange;
  int timechangestate;
  bool justchangetogreen;
};

int main ( void ) {
  int input;
  int stoplightcount = -1;
  int state = 0;
  vector<stoplight> stoplights;

  while( cin >> input ) {

    if( input == 0 ) {
      state++;
      if( state == 1 ) {
        //print how long
        int a;
        int duration = 0;
        int twice = 0;
        bool sync = false;
        // for( a = 0; a < stoplights.size(); a++ )
        //   cout << stoplights[a].green << endl;
        for( a = 0; a < 18000; a++ ) {
          int b;
          // cout << stoplights.size() << endl;
          // cout << a << endl;
          for( b = 0; b < stoplights.size(); b++ ) {
            stoplights[b].timechangestate++;
            if( stoplights[b].state == "red" && stoplights[b].red == stoplights[b].timechangestate ) {
              // cout << b << " " << a << endl;
              stoplights[b].state = "green";
              stoplights[b].timechangestate = 0;
              stoplights[b].justchangetogreen = true;
            }
            else if( stoplights[b].state == "green" && stoplights[b].green == stoplights[b].timechangestate ) {
              stoplights[b].justchangetogreen = false;
              stoplights[b].state = "orange";
              stoplights[b].timechangestate = 0;
            }
            else if( stoplights[b].state == "orange" && stoplights[b].orange == stoplights[b].timechangestate ) {
              stoplights[b].state = "red";
              stoplights[b].timechangestate = 0;
            }
          }
          bool allgreen = true;
          for( b = 0; b < stoplights.size(); b++ ) {
            if( stoplights[b].state != "green" )
              allgreen = false;
          }
          bool alljustturngreen = false;
          for( b = 0; b < stoplights.size(); b++ ) {
            if( stoplights[b].justchangetogreen == true ) {
              alljustturngreen = true;
              break;
            }
          }
          if( allgreen == true && alljustturngreen == true ) {
            // cout << "Hi." << endl;
            // for( b = 0; b < stoplights.size(); b++ )
            //   cout << "Vector: " << b << " " << a << " " << stoplights[b].state << endl;
            for( b = 0; b < stoplights.size(); b++ )
              stoplights[b].justchangetogreen = false;
            duration = a + 1;
            // cout << "Hi." << duration << endl;

            int durationhours, durationminutes, durationseconds;
            durationhours = duration / 3600;
            duration = duration % 3600;
            durationminutes = duration / 60;
            duration = duration % 60;
            if( durationhours < 10 )
              cout << '0';
            cout << durationhours << ":";
            if( durationminutes < 10 )
              cout << '0';
            cout << durationminutes << ":";
            if( duration < 10 )
              cout << '0';
            cout << duration << endl;
            sync = true;
            break;
          }
        }
        if( sync == false )
          cout << "Signals fail to synchronise in 5 hours" << endl;
        stoplights.clear();
        stoplightcount = -1;
      }
    }
    if( state == 4 )
      break;

    else if( input != 0 ) {
      state = 0;
      stoplightcount++;
      stoplights.push_back( stoplight() );
      stoplights[stoplightcount].state = "green";
      stoplights[stoplightcount].green = input - 5;
      stoplights[stoplightcount].red = input;
      stoplights[stoplightcount].orange = 5 ;
      stoplights[stoplightcount].timechangestate = 0;
      stoplights[stoplightcount].justchangetogreen = false;
    }
  }
  return 0;
}
