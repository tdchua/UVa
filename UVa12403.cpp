#include <iostream>
#include <string>
using namespace std;

int main(void) {

  int num_of_oper;
  cin >> num_of_oper;
  long long unsigned int donation = 0;
  for(int a = 0; a < num_of_oper; a++) {

    string command;
    cin >> command;

    if(command == "donate") {
      int amount;
      cin >> amount;

      donation += amount;
    }

    else if(command == "report") {

      cout << donation << endl;
    }
  }

  return 0;
}
