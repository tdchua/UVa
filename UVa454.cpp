#include <iostream>
#include <string>
#include <vector>
#include <ctype.h>
#include <algorithm>
using namespace std;

int main( void ) {

  int testcases;
  cin >> testcases;

  char nullchar;
  scanf( "%c", &nullchar );

  string buffer;
  getline( cin, buffer );
  int a;
  for( a = 0; a < testcases; a++ ) {
    if( a != 0 )
      cout <<  endl;

    vector<string> listofwords;
    listofwords.empty();

    while( getline( cin, buffer ) ) {

      // cout << buffer.length() << endl;
      if( buffer.length() == 0 )
        break;

      int b;
      bool placed = false;
      for( b = 0; b < listofwords.size(); b++ ) {
        if( lexicographical_compare( buffer.begin(), buffer.end(), listofwords[b].begin(), listofwords[b].end() ) == true ) {
          listofwords.insert( listofwords.begin() + b, buffer );
          placed = true;
          break;
        }
      }
      if( placed == false )
        listofwords.push_back( buffer );
    }

    int b;
    for( b = 0; b < listofwords.size(); b++ ) {
      string StringA = listofwords[b];
      int c;
      for( c = 0; c < StringA.length(); c++ ) {
        if( StringA[c] == ' ' ) {
          StringA.erase( c, 1 );
          c--;
        }
      }
      sort( StringA.begin(), StringA.end() );
      for( c = b + 1; c < listofwords.size(); c++ ) {

        int d;
        string StringB = listofwords[c];
        for( d = 0; d < StringB.length(); d++ ) {
          if( StringB[d] == ' ' ) {
            StringB.erase( d, 1 );
            d--;
          }
        }

        sort( StringB.begin(), StringB.end() );

        if( StringA == StringB )
          cout << listofwords[b] << " = " << listofwords[c] << endl;
      }
    }

  }
  return 0;
}
