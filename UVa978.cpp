#include <iostream>
#include <vector>
#include <algorithm>
#include <queue>
#include <set>
#include <utility>
using namespace std;

int main(void) {

  int testcases;
  cin >> testcases;

  int a;
  for(a = 0; a < testcases; a++) {

    if(a > 0)
      cout << endl;

    int B, SG, SB;
    cin >> B >> SG >> SB;

    multiset<int> green;
    multiset<int> blue;

    int b;
    int input;
    for(b = 0; b < SG; b++) {
      cin >> input;
      green.insert(input);
    }
    for(b = 0; b < SB; b++) {
      cin >> input;
      blue.insert(input);
    }

    queue<int> green_survivors;
    queue<int> blue_survivors;

    while(green.empty() == false && blue.empty() == false) {

      int diff;
      for(b = 0; b < B; b++) { //number of battlefields

        if(blue.empty() == true || green.empty() == true)
          break;

        diff = *(--green.end()) - *(--blue.end());
        if(diff == 0) { //Green and blue tied.
        }
        else if(diff < 0) { //Blue won
          diff = 0 - diff;
          blue_survivors.push(diff);
        }
        else if(diff > 0) { //Green won
          green_survivors.push(diff);
        }

        green.erase(--green.end());
        blue.erase(--blue.end());

      }
      while(blue_survivors.empty() == false) {
        blue.insert(blue_survivors.front());
        blue_survivors.pop();
      }
      while(green_survivors.empty() == false) {
        green.insert(green_survivors.front());
        green_survivors.pop();
      }

    }



    if(green.empty() == true && blue.empty() == true)
      cout << "green and blue died" << endl;

    else if(green.empty() == true && blue.empty() == false) {
      cout << "blue wins" << endl;
      for(auto it = blue.crbegin(); it != blue.crend(); it++)
        cout << *(it) << endl;
    }

    else if(green.empty() == false && blue.empty() == true) {
      cout << "green wins" << endl;
      for(auto it = green.crbegin(); it != green.crend(); it++)
        cout << *(it) << endl;
    }

  }
  return 0;
}
