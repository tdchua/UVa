#include <iostream>

using namespace std;

int main( void ) {

  int input;

  while( scanf( "%d", &input ) ) {

    if( input == 0 )
      break;

    int i = 0;
    int number = 0;
    int max = 0;
    int maxprime = 0;

    for( i = 0; i < input ; i++ ) {

      scanf( "%d", &number );

      if( max + number > 0 ) {
        max = max + number;

        if( maxprime < max )
          maxprime = max;
      }
      else
        max = 0;

    }

    if( max > 0 )
      cout << "The maximum winning streak is " << maxprime << '.' << endl;
    else
      cout << "Losing streak." << endl;

  }
}
