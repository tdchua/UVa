#include <iostream>
#include <string>
#include <vector>
#include <cstring>
#include <limits>

using namespace std;

//Brown,Green, Clear

//List of Combinations in Alphabetical Order:
// BCG
// BGC
// CBG
// CGB
// GBC
// GCB

//brown=0,green=1,clear=2
int color = 0;
int set[9];
int mincost = std::numeric_limits<int>::max();
int cost = 0;
int whichcombination = 0;
char combination[6][10] = {"BCG", "BGC", "CBG", "CGB", "GBC", "GCB"};

int main() {

  //Brown, Green , Clear
  // brown = 0 , 3 , 6
  // green = 1 , 4 , 7
  // clear = 2 , 5 , 8

  while (scanf("%d", &set[0]) == 1) {

    int i = 0;
    mincost = std::numeric_limits<int>::max();
    
    for ( i = 1 ; i < 9 ; i++ ) {
      scanf("%d", &set[i]);
    }

    for ( i = 0 ; i < 6 ; i++ ) {

      switch ( combination[i][0] ) {
        case 'B' : cost = set[3] + set[6]; break;
        case 'G' : cost = set[4] + set[7]; break;
        case 'C' : cost = set[5] + set[8]; break;
      }

      switch ( combination[i][1] ) {
        case 'B' : cost = cost + set[0] + set[6]; break;
        case 'G' : cost = cost + set[1] + set[7]; break;
        case 'C' : cost = cost + set[2] + set[8]; break;
      }

      switch ( combination[i][2] ) {
        case 'B' : cost = cost + set[0] + set[3]; break;
        case 'G' : cost = cost + set[1] + set[4]; break;
        case 'C' : cost = cost + set[2] + set[5]; break;
      }

      if ( mincost > cost ) {
        mincost = cost;
        whichcombination = i;
      }

    }

    cout << combination[whichcombination] << " " << mincost << endl;


  }
}
