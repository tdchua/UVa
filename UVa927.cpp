#include <iostream>
#include <math.h>
using namespace std;


int main( void ) {

  int testcases;
  int b;
  cin >> testcases;
  for( b = 0; b < testcases; b++ )
  {
    int degree;
    cin >> degree;

    int parameters[degree+1];
    int a;
    for( a = 0; a < degree + 1; a++ )
      cin >> parameters[a];

    float d;
    cin >> d;
    float k;
    cin >> k;
    float sum = 0;

    for( a = 1; ; a++ ) {
      sum = sum + ( a * d );
      if( sum >= k )
        break;
    }
    // cout << "A: " << a << endl;
    int n = a;
    double value = 0;
    for( a = 0; a < degree + 1; a++ ) {
      value = value + ( parameters[a] * pow( n, a) );
      // cout << "Value: " << value << endl;
    }
    printf( "%.0f\n", value );
  }
}
