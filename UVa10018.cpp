#include <iostream>
#include <cmath>

using namespace std;

int testcasenumber;
int i,j;
unsigned int number;
int numberofdigits;
unsigned int variabletocountnumofdigits;
unsigned int arrayofnumbers[10];
int togetdigit;
unsigned int reversed;
int numberofdigitscopy;
unsigned int sum;
int numberofiterations;

bool palindromecheck( unsigned int number ) {

  unsigned numberofdigitsfunction = 0;
  int functionarray[10];
  int j = 0;
  while( number != 0 ) {
    // cout << variabletocountnumofdigits % 10 << endl;
    functionarray[numberofdigitsfunction] = number % 10;
    number = number / 10;
    numberofdigitsfunction = numberofdigitsfunction + 1;
  }

  for( j = 0; j < numberofdigitsfunction / 2; j++ ) {

    if( functionarray[j] != functionarray[ numberofdigitsfunction - 1 - j ] )
      return 0;

  }

  return 1;
}



int main(void) {

  while( scanf("%d", &testcasenumber) == 1 ) {

    for( i = 0 ; i < testcasenumber ; i++ ) {
      numberofiterations = 0;
      reversed = 0;
      numberofdigits = 0;
      sum = 0;
      scanf( "%d", &number );
      variabletocountnumofdigits = number;
      sum = number;

      while( numberofiterations < 100 ) {

        numberofiterations = numberofiterations + 1;
        numberofdigits = 0;
        reversed = 0;

        while( variabletocountnumofdigits != 0 ) {
          //cout << variabletocountnumofdigits % 10 << endl;
          arrayofnumbers[numberofdigits] = variabletocountnumofdigits % 10;
          variabletocountnumofdigits = variabletocountnumofdigits / 10;
          numberofdigits = numberofdigits + 1;
        }
        numberofdigitscopy = numberofdigits;
        for ( j = 0 ; j < numberofdigits ; j++ ) {
          reversed = reversed + ( arrayofnumbers[j] * pow( 10, numberofdigitscopy - 1));
          numberofdigitscopy = numberofdigitscopy - 1;
        }
        // cout << "Sum: " << sum << " " << "Reversed: " << reversed << endl;
        sum = sum + reversed;
        // cout << "Sum after adding: " << sum << endl;
        if ( palindromecheck(sum) == 1 ) {
          //output minimum number of iterations, resulting palindrome
          cout << numberofiterations << " " << sum << endl;
          break;
        }
        variabletocountnumofdigits = sum;
      }
    }
  }
}
