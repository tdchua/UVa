#include <iostream>
#include <map>
#include <vector>
#include <algorithm>
#include <string>
#include <iterator>

using namespace std;

int main(void) {

  int testcases = 0;
  int a;

  scanf("%d\n", &testcases);

  for(a = 0; a < testcases; ) {

    if(a > 0)
      printf("\n");

    string input;
    char newlinebuffer;
    map<string,int> trees;
    vector<string> listoftrees;
    float number_of_trees = 0;
    bool newline = false;

    while(std::getline(cin, input)) {

      if(input.size() == 0) { //checking blank line
        a = a + 1;
        newline = true;
        break;
      }

      else {
        number_of_trees = number_of_trees + 1;
        map<string,int>::iterator point_me = trees.find(input);
        if(point_me == trees.end()) {
          trees[input] = 1;
          listoftrees.push_back(input);
        }
        else {
          // cout << "Located: " << input << endl;
          trees[input] += 1;
        }
      }
    }
    if(newline == false)
      a = a + 1;

    sort(listoftrees.begin(),listoftrees.end());
    int b;
    for(b = 0; b < listoftrees.size(); b++) {

      cout << listoftrees[b] << " ";
      float answer;
      answer = (trees[listoftrees[b]] / (number_of_trees)) * 100;
      printf("%.4f\n", answer);

    }

  }

  return 0;
}
