#include <iostream>
#include <queue>
#include <string>

using namespace std;

int main(void) {

  int testcases;
  cin >> testcases;

  int a;
  for(a = 0; a < testcases; a++) {
    if(a > 0)
      cout << endl;
    queue<int> right;
    queue<int> left;
    queue<int> value;
    queue<bool> order_direction;
    queue<int> buffer_value;
    queue<bool> buffer_direction;
    queue<int> buffer_answer;
    int car_capacity;
    int time_it_takes;
    int lines;
    cin >> car_capacity >> time_it_takes >> lines;

    int b;
    for(b = 0; b < lines; b++) {

      int time_arrival;
      string direction;
      cin >> time_arrival >> direction;
      value.push(time_arrival);
      (direction == "right") ? right.push(time_arrival) : left.push(time_arrival);
      (direction == "right") ? order_direction.push(false) : order_direction.push(true);

    }

    int currenttime = 0;
    bool ferry_on_left = true;
    bool car_on_left_first = true;
    queue<int> car_on_board;

    while(right.empty() == false || left.empty() == false || value.empty() == false) {


      // if(buffer_value.empty() == false)
      //   cout << buffer_value.front() << " " << buffer_direction.front() << endl;


      if(right.empty() == true) {//there are only cars on the left
        if(left.empty() == false) {

          if(currenttime < left.front())
            currenttime = left.front();

          if(ferry_on_left == false)
            currenttime += time_it_takes;

          while(left.front() <= currenttime && car_on_board.size() < car_capacity) {

            car_on_board.push(left.front());
            left.pop();
            if(left.empty() == true)
              break;
          }

          currenttime += time_it_takes;

          // cout << left.size() << endl;
          ferry_on_left = false;
        }

      }
      else if(left.empty() == true) {//there are only cars on the right
        if(right.empty() == false) {

          if(currenttime < right.front())
            currenttime = right.front();
          if(ferry_on_left == true)
            currenttime += time_it_takes;


          while(right.front() <= currenttime && car_on_board.size() < car_capacity) {
            car_on_board.push(right.front());
            right.pop();
            if(right.empty() == true)
              break;
          }
          currenttime += time_it_takes;

          ferry_on_left = true;
        }
      }


      else { //there are cars on both sides

        if(right.empty() == false && left.empty() == false) {
          // cout << "Current Time: " << currenttime << endl;
          if((ferry_on_left == true && currenttime < left.front()) || (ferry_on_left == false && currenttime < right.front())) {
            // cout << "Hi." << endl;
            if(car_on_board.empty() == true && ((ferry_on_left == false && (left.front() < right.front())) || (ferry_on_left == true && (left.front() > right.front())))) {
              if(left.front() < right.front()) {
                if(currenttime < left.front())
                  currenttime = time_it_takes + (left.front());
                else
                  currenttime = time_it_takes + currenttime;
              }
              else if(left.front() > right.front()) {
                if(currenttime < right.front())
                  currenttime = time_it_takes + (right.front());
                else
                  currenttime = time_it_takes + currenttime;
              }

              ferry_on_left = !ferry_on_left;
            }
          }
          if(ferry_on_left == true) {
            if(currenttime < left.front())
              currenttime = left.front();

            while(left.front() <= currenttime && car_on_board.size() < car_capacity) {

              car_on_board.push(left.front());
              left.pop();
              if(left.empty() == true)
                break;
            }
            currenttime += time_it_takes;
            ferry_on_left = false;
          }

          else if(ferry_on_left == false) {
            if(currenttime < right.front())
              currenttime = right.front();

            while(right.front() <= currenttime && car_on_board.size() < car_capacity) {

              car_on_board.push(right.front());
              right.pop();
              if(right.empty() == true)
                break;
            }
            currenttime += time_it_takes;
            ferry_on_left = true;

          }
        }
      }

      // cout << buffer_value.size() << endl;

      for(b = 0; b < car_on_board.size(); ) {
        // cout << value.front() << " " << order_direction.front() << endl;
        // cout << car_on_board.front() << endl;
        if(car_on_board.front() == value.front() && ferry_on_left != order_direction.front()) {
          // cout << "Car dropped: " << car_on_board.front() << endl;
          cout << currenttime << endl;
          car_on_board.pop();
          value.pop();
          order_direction.pop();

        }
        else {
          // cout << "Car on Board: " << car_on_board.front() << endl;
          if(buffer_value.empty() == false) {
            // cout << "Buffer Value: " << buffer_value.front() << endl;
            if(car_on_board.front() != value.front() && ferry_on_left == order_direction.front()) {
              // cout << "Add to Queue: " << car_on_board.front() << endl;
              buffer_value.push(car_on_board.front());
              buffer_direction.push(ferry_on_left);
              buffer_answer.push(currenttime);
            }
            if(buffer_value.front() == value.front() && buffer_direction.front() != order_direction.front()) {
              // cout << "Car dropped: " << buffer_value.front() << endl;
              cout << buffer_answer.front() << endl;
              // cout << buffer_value.front() << endl;
              buffer_value.pop();
              buffer_direction.pop();
              buffer_answer.pop();
              value.pop();
              order_direction.pop();
            }
            else {
              // cout << "Added to queue: " << car_on_board.front() << endl;
              buffer_value.push(car_on_board.front());
              buffer_direction.push(ferry_on_left);
              buffer_answer.push(currenttime);
            }
          }
          else {
            // cout << "Add to Queue: " << car_on_board.front() << endl;
            buffer_value.push(car_on_board.front());
            buffer_direction.push(ferry_on_left);
            buffer_answer.push(currenttime);
          }
          car_on_board.pop();
        }
      }
      if(buffer_value.empty() == false) {
        while(buffer_value.front() == value.front() && buffer_direction.front() != order_direction.front()) {
          cout << buffer_answer.front() << endl;
          // cout << buffer_value.front() << endl;
          buffer_value.pop();
          buffer_direction.pop();
          buffer_answer.pop();
          value.pop();
          order_direction.pop();
        }
      }
    }
  }
  return 0;
}
