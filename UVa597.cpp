#include <iostream>
#include <string>
#include <cmath>

using namespace std;

int main( void ) {

  int hours, minutes;

  while( scanf("%d",&hours) == 1 ) {

      scanf(":");
      cin >> minutes;
      if( hours == 0 && minutes == 0 )
        return 0;

      float hoursangle,minutesangle;

      // 1 minute = 0.5 degrees
      hoursangle = ( hours * 30 ) + ( 0.5 * minutes );
      minutesangle = ( minutes * 6 );

      float diffone, difftwo;

      if( hoursangle >= minutesangle ) {
        diffone = hoursangle - minutesangle;
        difftwo = ( 360 - hoursangle ) + minutesangle;
      }

      if( hoursangle < minutesangle ) {
        diffone = minutesangle - hoursangle;
        difftwo = ( 360 - minutesangle ) + hoursangle;
      }

      if ( diffone > difftwo )
        printf("%.3f\n",difftwo);
      else
        printf("%.3f\n",diffone);

  }
}
