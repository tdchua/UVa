#include <iostream>
#include <string>
#include <sstream>

using namespace std;

struct tim_t {  //blocks

  int location;
  int position;
  int peak;

};

int main( void ) {

  string input;
  int number;
  int i;
  int j;
  char filler;

  scanf( "%d", &number );

  tim_t arrayoftimt[number];

  for( i = 0; i < number; i++ ) {

    arrayoftimt[i].location = i;
    arrayoftimt[i].position = 0;

  }

  scanf( "%c", &filler );

  while( getline( cin, input) ) {

    if( input == "quit" )
      break;

    else {

      istringstream iss( input );


      string firstword;
      string secondword;
      int firstnumber;
      int secondnumber;
      int peak;


      iss >> firstword >> firstnumber >> secondword >> secondnumber;

      if( firstnumber == secondnumber || arrayoftimt[firstnumber].location == arrayoftimt[secondnumber].location )
        continue;

      if( firstword == "move" ) {

        if( secondword == "onto" ) {

          for( i = 0; i < number; i++ ) {

            //Clear blocks on top of A
            if( arrayoftimt[i].location == arrayoftimt[firstnumber].location && arrayoftimt[i].position > arrayoftimt[firstnumber].position ) {

              arrayoftimt[i].position = 0;
              arrayoftimt[i].location = i;

            }

            //Clear blocks on top of B
            else if( arrayoftimt[i].location == arrayoftimt[secondnumber].location && arrayoftimt[i].position > arrayoftimt[secondnumber].position ) {

              arrayoftimt[i].position = 0;
              arrayoftimt[i].location = i;

            }

          }

          arrayoftimt[firstnumber].location = arrayoftimt[secondnumber].location;
          arrayoftimt[firstnumber].position = arrayoftimt[secondnumber].position + 1;

        }

        else if( secondword == "over" ) {

          for( i = 0; i < number; i++ ) {

            if( arrayoftimt[i].location == arrayoftimt[firstnumber].location && arrayoftimt[i].position > arrayoftimt[firstnumber].position ) {

              arrayoftimt[i].position = 0;
              arrayoftimt[i].location = i;

            }
          }

          arrayoftimt[firstnumber].location = arrayoftimt[secondnumber].location;
          peak = -1;

          for( i = 0; i < number; i++ ) {

            if( arrayoftimt[i].location == arrayoftimt[secondnumber].location && arrayoftimt[i].position > peak )
              peak = arrayoftimt[i].position;

          }


          arrayoftimt[firstnumber].position = peak + 1;


        }
      }

      else if( firstword == "pile" ) {

        if( secondword == "onto" ) {

          for( i = 0; i < number; i++ ) { //Taking out blocks on top of B

            if( arrayoftimt[i].location == arrayoftimt[secondnumber].location && arrayoftimt[i].position > arrayoftimt[secondnumber].position ) {

              arrayoftimt[i].position = 0;
              arrayoftimt[i].location = i;

            }

          }

          j = 0;

          for( i = 0; i < number; i++ ) { //Moving blocks on top of block A

            if( arrayoftimt[i].location == arrayoftimt[firstnumber].location && arrayoftimt[i].position > arrayoftimt[firstnumber].position ) {

              arrayoftimt[i].location = arrayoftimt[secondnumber].location;
              arrayoftimt[i].position = arrayoftimt[i].position - arrayoftimt[firstnumber].position + arrayoftimt[secondnumber].position + 1;

            }
          }

          arrayoftimt[firstnumber].location = arrayoftimt[secondnumber].location;
          arrayoftimt[firstnumber].position = arrayoftimt[secondnumber].position + 1;

        }

        else if( secondword == "over" ) {

          peak = -1;

          for( i = 0; i < number; i++ ) {

            if( arrayoftimt[i].location == arrayoftimt[secondnumber].location && arrayoftimt[i].position > peak )
              peak = arrayoftimt[i].position;

          }
          peak = peak + 1;

          if( peak == -1 )
            peak = 0;

          for( i = 0; i < number; i++ ) { //Moving blocks on top of block A

            if( arrayoftimt[i].location == arrayoftimt[firstnumber].location && arrayoftimt[i].position > arrayoftimt[firstnumber].position ) {

              arrayoftimt[i].location = arrayoftimt[secondnumber].location;
              arrayoftimt[i].position = arrayoftimt[i].position - arrayoftimt[firstnumber].position + peak;

            }
          }

          arrayoftimt[firstnumber].location = arrayoftimt[secondnumber].location;
          arrayoftimt[firstnumber].position = peak;

        }
      }

    }
  }


  for( i = 0; i < number; i++ ) {

    cout << i << ':';
    int limit = 0;

    for( j = 0; j < number; j++ ) {

      if( arrayoftimt[j].location == i && arrayoftimt[j].position == limit ) {

        cout << ' ' << j;
        limit = limit + 1;
        j = -1;

      }
    }
    cout << endl;
  }

  return 0;
}
