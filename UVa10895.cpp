#include <iostream>
#include <queue>
using namespace std;

int main(void) {

  int m, n;

  while(cin >> m >> n) {

    int my_matrix[n][m];
    int num_of_indices;

    int b;

    for(b = 0; b < m; b++) {
      // cout << "Hi." << " B: " << b << endl;
      cin >> num_of_indices;
      int a;
      queue<int> indices;
      for(a = 0; a < num_of_indices; a++) {
        int index;
        cin >> index;
        indices.push(index);
      }
      for(a = 0; a < n; a++) {
        int value;
        if(indices.size() != 0) {
          cin >> value;
          while(a != indices.front() - 1) {
            my_matrix[a][b] = 0;
            a = a + 1;
          }
          if(a == indices.front() - 1) {
            my_matrix[a][b] = value;
            indices.pop();
          }
        }

        else if(indices.size() == 0) {
          my_matrix[a][b] = 0;
        }
      }
    }
    int c;
    // cout << "New matrix" << endl;
    // for(b = 0; b < n; b++) {
    //   for(c = 0; c < m; c++) {
    //     cout << my_matrix[b][c] << " ";
    //   }
    //   cout << endl;
    // }

    cout << n << " " << m << endl;
    for(b = 0; b < n; b++) {

      num_of_indices = 0;
      queue<int> indices;
      queue<int> values;
      for(c = 0; c < m; c++) {
        if(my_matrix[b][c] != 0) {
          num_of_indices++;
          indices.push(c);
          values.push(my_matrix[b][c]);
        }
      }
      cout << num_of_indices;
      for(c = 0; c < num_of_indices; c++) {
        cout << " " << indices.front() + 1;
        indices.pop();
      }
      cout << endl;
      for(c = 0; c < num_of_indices; c++) {
        if(c > 0)
          cout << " ";
        cout << values.front();
        values.pop();
      }
      cout << endl;
    }
  }
  return 0;
}
